/* Copyright (c) 2002 Dirk Jagdmann <doj@cubic.org>, Michael Stickel <michael@stickel.org> */

/* This software is provided 'as-is', without any express or implied */
/* warranty. In no event will the authors be held liable for any damages */
/* arising from the use of this software. */

/* Permission is granted to anyone to use this software for any purpose, */
/* including commercial applications, and to alter it and redistribute it */
/* freely, subject to the following restrictions: */

/*     1. The origin of this software must not be misrepresented; you */
/*        must not claim that you wrote the original software. If you use */
/*        this software in a product, an acknowledgment in the product */
/*        documentation would be appreciated but is not required. */

/*     2. Altered source versions must be plainly marked as such, and */
/*        must not be misrepresented as being the original software. */

/*     3. This notice may not be removed or altered from any source */
/*        distribution. */

/* $Header: /code/bslsh/disassembler.cc,v 1.15 2010/05/22 01:23:26 doj Exp $ */

#include <cctype>

#include <iostream>
#include <sstream>
#include <string>

#include "disassembler.hh"
#include "registername.hh"

using namespace std;

/*------ Anything nessesary about addressing modes --------
 *
 * +-------------------------------+
 * |  Double Operand Instructions  |
 * +----------+---------+-----+----+
 * |  As      |  Ad     | len | cy |
 * +----------+---------+-----+----+
 * | 00,Rn    | 0,Rm    | 1   | 1  |
 * | 00,Rn    | 0,PC    | 1   | 2  |
 * +----------+---------+-----+----+
 * | 00,Rn    | 1,x(Rm) | 2   | 4  |
 * | 00,Rn    | 1,EDE   | 2   | 4  |
 * | 00,Rn    | 1,&EDE  | 2   | 4  |
 * +----------+---------+-----+----+
 * | 01,x(Rn) | 0,Rm    | 2   | 3  |
 * | 01,EDE   | 0,Rm    | 2   | 3  |
 * | 01,&EDE  | 0,Rm    | 2   | 3  |
 * +----------+---------+-----+----+
 * | 01,x(Rn) | 1,x(Rm) | 3   | 6  |
 * | 01,EDE   | 1,TONI  | 3   | 6  |
 * | 01,&EDE  | 1,TONI  | 3   | 6  |
 * +----------+---------+-----+----+
 * | 10,@Rn   | 0,Rm    | 1   | 2  |
 * +----------+---------+-----+----+
 * | 10,@Rn   | 1,x(Rm) | 2   | 5  |
 * | 10,@Rn   | 1,EDE   | 2   | 5  |
 * | 10,@Rn   | 1,&EDE  | 2   | 5  |
 * +----------+---------+-----+----+
 * | 11,@Rn+  | 0,Rm    | 1   | 2  |
 * | 11,@Rn+  | 0,PC    | 1   | 3  |
 * | 11,#N    | 0,Rm    | 2   | 2  |
 * | 11,#N    | 0,PC    | 2   | 3  |
 * +----------+---------+-----+----+
 * | 11,@Rn+  | 1,x(Rm) | 2   | 5  |
 * | 11,#N    | 1,EDE   | 3   | 5  |
 * | 11,@Rn+  | 1,&EDE  | 2   | 5  |
 * | 11,#N    | 1,&EDE  | 3   | 5  |
 * +----------+---------+-----+----+
 *
 *
 *
 * +-------------------------------+
 * |  Single Operand Instructions  |
 * +----------+-------+------------+
 * |  A(s/d)  |  len  | cycles     |
 * +----------+-------+------------+
 * | 00,Rn    |  1    |            |
 * +----------+-------+------------+
 * | 01,X(Rn) |  2    |            |
 * | 01,EDE   |  2    |            |
 * | 01,&EDE  |  2    |            |
 * +----------+-------+------------+
 * | 10,@Rn   |  1    |            |
 * +----------+-------+------------+
 * | 11,@Rn+  |  1    |            |
 * | 11,#N    |  2    |            |
 * +----------+-------+------------+
 *
 *
 *-----------------------------------------------------------------------------------+
 * Complete Addressing modes matrix (more complete as in the Datasheet)              |
 *-----------------------------------------------------------------------------------+
 * As / Ad sreg/dreg  Addressing        Syntax Description                           |
 * ------- ---------  --------------    ------ --------------------------------------+
 * 00 / 0   xxxx      Register Mode     Rn     Register contents are operand         |
 * 00 / 0   0003      Constant 0x0000   0x0000 R3                                    |
 *                                                                                   |
 *-----------------------------------------------------------------------------------+
 *                                                                                   |
 * 01 / 1   xxxx      Indexed Mode      X(Rn)  (Rn + X) points to the operand,       |
 *                                             X is stored in the next word.         |
 *                                                                                   |
 * 01 / 1   0000      Symbolic Mode     ADDR   (PC + X) points to the operand.       |
 *                                             X is stored in the next word.         |
 *                                             indexed mode X(PC) is used.           |
 *                                             (R0 + X)                              |
 *                                                                                   |
 * 01 / 1   0002      Absolute Mode     &ADDR  The word following the instruction    |
 *                                             contains the absolute address.        |
 *                                             (R2 + X)                              |
 *                                                                                   |
 * 01 / 1   0003      Constant 0x0001   0x0001 This addressing mode does not have    |
 *                                             an extra operand. Constant register   |
 *                                             R3 is used.                           |
 *                                                                                   |
 *-----------------------------------------------------------------------------------+
 *                                                                                   |
 * 10 / -   xxxx      Indirect Register @Rn    Rn is used as a pointer to the        |
 *                    Mode                     operand                               |
 *                                                                                   |
 * 10 / -   0002      Constant 0x0004   0x0004 The operand is a constant.            |
 *                                                                                   |
 * 10 / -   0003      Constant 0x0002   0x0002 The operand is a constant.            |
 *                                                                                   |
 *-----------------------------------------------------------------------------------+
 *                                                                                   |
 * 11 / -   xxxx      Indirect          @Rn+   Rn is used as a pointer to the        |
 *                    Autoincrement            operand. Rn is incremented afterwards.|
 *                                                                                   |
 * 11/-     0000      Imediate mode     #N     The word following the instruction    |
 *                                             contains the immediate constant       |
 *                                             N. Indirect autoincrement mode        |
 *                                             @PC+ is used (@R0+).                  |
 *                                                                                   |
 * 11/-     0002      Constant 0x0008   0x0008 The operand is a constant.            |
 *                                                                                   |
 * 11/-     0003      Constant 0xFFFF   0xFFFF The operand is a constant.            |
 *                                                                                   |
 *-----------------------------------------------------------------------------------+
 *
 * Constant registers
 * Reg As
 * R2  00 -> Register access
 * R2  01 -> (0) Absolute address mode
 * R2  10 -> 0x0004
 * R2  11 -> 0x0008
 * R3  00 -> 0x0000
 * R3  01 -> 0x0001
 * R3  10 -> 0x0002
 * R3  11 -> 0xFFFF
 */

namespace MSP430 {

  const char *Disassembler::registerNames[16] = {
    "PC", "SP", "SR", "R3",
    "R4", "R5", "R6", "R7",
    "R8", "R9", "R10","R11",
    "R12","R13","R14","R15"
  };

  const char *Disassembler::twoOpNames[16] = {
    NULL, NULL, NULL, NULL,
    "MOV", "ADD", "ADDC", "SUBC",
    "SUB", "CMP", "DADD", "BIT",
    "BIC", "BIS", "XOR", "AND" 
  };

  const char *Disassembler::singleOpNames[8] = {
    "RRC", "SWPB", "RRA", "SXT",
    "PUSH", "CALL", "RETI", NULL 
  };

  const char *Disassembler::jumpOpNames[8] = {
    "JNE/JNZ", "JEQ/JZ", "JNC", "JC",
    "JN", "JGE", "JL", "JMP"
  };

  int Disassembler::decode_operand(const int am, const int reg, ostream& os)
  {
    const WORD oldpc=pc;
    lastabsoluteaddress=-1;

    switch (am&3)
      {
      case 0:
	if (reg==3)
	  {
	    HEX(os, 0);
	    lastoperand=Const0;
	    lastaddressingmode=ConstantMode;
	  }
	else
	  {
	    os << registerNames[reg];
	    lastoperand=Register;
	    lastaddressingmode=RegisterMode;
	  }
	break;

      case 1:
	switch(reg)
	  {
	  case 0:  
	    HEX(os, BE2HOST(fetch())); 
	    lastoperand=Number; 
	    lastaddressingmode=SymbolicMode;
	    break;
	  case 2:
	    {
	      WORD a=BE2HOST(fetch());
		os << "&"; HEX(os, a); 
	      lastoperand=Number; 
	      lastaddressingmode=AbsoluteMode;
	      lastabsoluteaddress=a;
	    }
	    break;
	  case 3:  
	    HEX(os, 1); 
	    lastoperand=Const1; 
	    lastaddressingmode=ConstantMode;
	    break;
	  default: 
	    HEX(os, BE2HOST(fetch())) << "(" << registerNames[reg] << ")"; 
	    lastoperand=Register; 
	    lastaddressingmode=IndexedMode;
	    break;
	  }
	break;

      case 2:
	switch (reg)
	  {
	  case 2: 
	    HEX(os, 4); 
	    lastoperand=Const4; 
	    lastaddressingmode=ConstantMode;
	    break;
	  case 3: 
	    HEX(os, 2); 
	    lastoperand=Const2; 
	    lastaddressingmode=ConstantMode;
	    break;
	  default: 
	    os << "@" << registerNames[reg]; 
	    lastoperand=Register; 
	    lastaddressingmode=IndirectMode;
	    break;
	  }
	break;

      case 3:
	switch(reg)
	  {
	  case 0: 
	    os << "#"; 
	    HEX(os, BE2HOST(fetch())); 
	    lastoperand=Number; 
	    lastaddressingmode=ImediateMode;
	    break;
	  case 2: 
	    HEX(os, 8); 
	    lastoperand=Const8; 
	    lastaddressingmode=ConstantMode;
	    break;
	  case 3: 
	    HEX(os, 0xFFFF); 
	    lastoperand=Constm1; 
	    lastaddressingmode=ConstantMode;
	    break;
	  default: 
	    os << "@" << registerNames[reg] << "+"; 
	    lastoperand=Register; 
	    lastaddressingmode=AutoincrementMode;
	    break;
	  }
	break;
      }

    return pc-oldpc;
  }

  int Disassembler::disassemble(ostream& os, const int offset)
  {
    if(offset>=0 && offset<=0xFFFF)
      pc=offset;

    if(pc&1)
      {
	clog << "disassemble(): program counter on odd address! aborting..." << endl;
	return -1;
      }

    if(pc>=0xFFFD)
      return -1;

    currentcycle=0;
    const WORD oldpc=pc;

    ostringstream out;

    const WORD code = BE2HOST(fetch());
    const int opcode = (code>>12) & 0xf;

    if(twoOpNames[opcode])	// two operands instructions
      {
	const int sreg   = (code>>8) & 0xf;
	const int ad     = (code>>7) & 1;
	const int b_w    = (code>>6) & 1;
	const int as     = (code>>4) & 3;
	const int dreg   = code & 0xf;

	// calculate cylces needed
	switch(as)
	  {
	  case 0:
	    if(ad==0)
	      {
		if(dreg==PC)
		  currentcycle=2;
		else
		  currentcycle=1;
	      }
	    else
	      currentcycle=4;
	    break;
	  case 1:
	    if(ad==0)
	      currentcycle=3;
	    else
	      currentcycle=6;
	    break;
	  case 2:
	    if(ad==0)
	      currentcycle=2;
	    else
	      currentcycle=5;
	    break;
	  case 3:
	    if(ad==0)
	      {
		if(dreg==PC)
		  currentcycle=3;
		else
		  currentcycle=2;
	      }
	    else
	      currentcycle=5;
	    break;
	  }

	out << currentcycle << " ";      
	out << twoOpNames[opcode] << ".";
	out << (b_w?"b":"w") << "\t";

	if (decode_operand(as, sreg, out) < 0)
	  cerr << endl << "Error in decoding first operand" << endl;
	const operand_t op1=lastoperand;
	const int op1mode=lastaddressingmode;

	out << ", ";

	const int inc=decode_operand(ad, dreg, out);
	if (inc < 0)
	  cerr << endl << "Error in decoding second operand" << endl;
	const operand_t op2=lastoperand;
	const int op2mode=lastaddressingmode;

	// try to find any emulated opcodes
	if(op1==Const0 && opcode==ADDC)
	  {
	    out << "\t= ADC.";
	    out << (b_w?"b":"w") << " ";
	    pc-=inc;
	    decode_operand(ad, dreg, out);
	  }
	else if(op1==Const0 && opcode==DADD)
	  {
	    out << "\t= DADC.";
	    out << (b_w?"b":"w") << " ";
	    pc-=inc;
	    decode_operand(ad, dreg, out);
	  }
	else if(op1==Const1 && opcode==SUB)
	  {
	    out << "\t= DEC.";
	    out << (b_w?"b":"w") << " ";
	    pc-=inc;
	    decode_operand(ad, dreg, out);
	  }
	else if(op1==Const2 && opcode==SUB)
	  {
	    out << "\t= DECD.";
	    out << (b_w?"b":"w") << " ";
	    pc-=inc;
	    decode_operand(ad, dreg, out);
	  }
	else if(op1==Const1 && opcode==ADD)
	  {
	    out << "\t= INC.";
	    out << (b_w?"b":"w") << " ";
	    pc-=inc;
	    decode_operand(ad, dreg, out);
	  }
	else if(op1==Const2 && opcode==ADD)
	  {
	    out << "\t= INCD.";
	    out << (b_w?"b":"w") << " ";
	    pc-=inc;
	    decode_operand(ad, dreg, out);
	  }
	else if(op1==Const0 && opcode==SUBC)
	  {
	    out << "\t= SBC.";
	    out << (b_w?"b":"w") << " ";
	    pc-=inc;
	    decode_operand(ad, dreg, out);
	  }

	else if(op1==Constm1 && opcode==XOR)
	  {
	    out << "\t= INV.";
	    out << (b_w?"b":"w") << " ";
	    pc-=inc;
	    decode_operand(ad, dreg, out);
	  }
	// RLA and RLC missing

	else if(op1==Const0 && opcode==MOV && op2==Const0)
	  {
	    out << "\t= NOP";
	  }
	else if(op1==Const0 && opcode==MOV)
	  {
	    out << "\t= CLR.";
	    out << (b_w?"b":"w") << " ";
	    pc-=inc;
	    decode_operand(ad, dreg, out);
	  }
	else if(op1==Const1 && opcode==BIC && dreg==SR && op2mode==RegisterMode)
	  {
	    out << "\t= CLRC";
	  }
	else if(op1==Const4 && opcode==BIC && dreg==SR && op2mode==RegisterMode)
	  {
	    out << "\t= CLRN";
	  }
	else if(op1==Const2 && opcode==BIC && dreg==SR && op2mode==RegisterMode)
	  {
	    out << "\t= CLRZ";
	  }
	else if(opcode==MOV && sreg==SP && dreg==PC && op1mode==AutoincrementMode)
	  {
	    out << "\t= RET";
	  }
	else if(opcode==MOV && sreg==SP && op1mode==AutoincrementMode)
	  {
	    out << "\t= POP ";
	    pc-=inc;
	    decode_operand(ad, dreg, out);
	  }
	else if(op1==Const1 && opcode==BIS && dreg==SR && op2mode==RegisterMode)
	  {
	    out << "\t= SETC";
	  }
	else if(op1==Const4 && opcode==BIS && dreg==SR && op2mode==RegisterMode)
	  {
	    out << "\t= SETN";
	  }
	else if(op1==Const2 && opcode==BIS && dreg==SR && op2mode==RegisterMode)
	  {
	    out << "\t= SETZ";
	  }
	else if(op1==Const0 && opcode==CMP)
	  {
	    out << "\t= TST.";
	    out << (b_w?"b":"w") << " ";
	    pc-=inc;
	    decode_operand(ad, dreg, out);
	  }

	else if(opcode==MOV && dreg==PC)
	  {
	    out << "\t= BR";
	  }
	else if(op1==Const8 && opcode==BIC && dreg==SR)
	  {
	    out << "\t= DINT";
	  }
	else if(op1==Const8 && opcode==BIS && dreg==SR)
	  {
	    out << "\t= EINT";
	  }
      }

    else if(opcode==1)		// one operand instructions
      {
	const int b_w  = (code>>6) & 1;
	const int ad   = (code>>4) & 3;
	const int dreg = code & 0xf;
	const int op   = (code>>7) & 0x1f;

	// calculate cycles
	if(op==RETI)
	  currentcycle=5;
	else
	  switch(ad)
	    {
	    case 0:
	      switch(op)
		{
		case RRA:
		case RRC:
		case SWPB:
		case SXT:
		  currentcycle=1;
		  break;
		case PUSH:
		  currentcycle=3;
		  break;
		case CALL:
		  currentcycle=4;
		  break;
		}
	      break;
	    case 1: 
	      switch(op)
		{
		case RRA:
		case RRC:
		case SWPB:
		case SXT:
		  currentcycle=4;
		  break;
		case PUSH:
		case CALL:
		  currentcycle=5;
		  break;
		}
	      break;
	    case 2:
	      switch(op)
		{
		case RRA:
		case RRC:
		case SWPB:
		case SXT:
		  currentcycle=3;
		  break;
		case PUSH:
		case CALL:
		  currentcycle=4;
		  break;
		}
	      break;
	    case 3:
	      switch(op)
		{
		case RRA:
		case RRC:
		case SWPB:
		case SXT:
		  currentcycle=3;
		  break;
		case PUSH:
		  currentcycle=4;
		  break;
		case CALL:
		  currentcycle=5;
		  break;
		}
	      break;
	    }      

	const char *cmd = singleOpNames[op];
	if (cmd)
	  {
	    out << currentcycle << " ";      
	    out << cmd << ".";
	    out << (b_w?"b":"w") << "\t";
	    if(decode_operand(ad, dreg, out) < 0)
	      cerr << endl << "Error in decoding operand" << endl;
	  }
	else
	  out << "illegal single operand instruction";
      }

    else if ((opcode>>1) == 1)   // branch instructions
      {
	currentcycle=2;

	int offs = code&0x1ff;
	if (code&0x200)
	  offs = -((~offs&0x1ff)+1);
	offs*=2;
	const WORD dst=pc+offs;
	jmpdst.insert(dst);

	out << currentcycle << " ";      
	out << jumpOpNames[(code>>10)&7] << "\t";
	if(offs<0)
	  {
	    out << "-";
	    HEX(out, -offs) << " -> ";
	  }
	else
	  {
	    out << " ";
	    HEX(out, offs) << " -> ";
	  }
	HEX(out, dst);
      }

    else
      {
	out << "???";
      }

    const int bytesused=pc-oldpc;
    cycles+=currentcycle;

    for(int i=0; i<bytesused; i++)
      {
	const BYTE *mem=reinterpret_cast<BYTE*>(memory);      
	const BYTE b=mem[oldpc+i];
	os << static_cast<char>(isprint(b)?b:'.');
      }
    os << "\t";

    for(unsigned int i=0; i<3; i++)
      if(i<bytesused/sizeof(WORD))
	HEX(os, memory[oldpc/sizeof(WORD)+i]) << " ";
      else
	os << "     ";
    os << "\t";

    os << out.str();

    if(lastabsoluteaddress>=0)
      for(int i=0; RegisterName[i].name; i++)
	if(RegisterName[i].offset==lastabsoluteaddress)
	  {
	    os << "\t";
	    HEX(os, lastabsoluteaddress) << " = " << RegisterName[i].name;
	    break;
	  }

    return bytesused;
  }


  int Disassembler::disassemble(int len, const int addr, ostream& os)
  {
    if(addr>=0)
      {
	if(addr&1)
	  {
	    cerr << "disassemble(): addr is odd! aborting..." << endl;
	    return -1;
	  }
	pc=addr;
      }

    if(pc+len>0xFFFF)
      {
	clog << "disassemble(): len exceeds max memory of 0xFFFF. Truncating length" << endl;
	len=0xFFFF-pc;
      }

    cycles=0;
    const WORD oldpc=pc;
    while(pc<oldpc+len)
      {
	if(pc>=0xFFFD)
	  break;

	const WORD curpc=pc;

	ostringstream out;
	if(disassemble(out) < 0)
	  break;

	if(jmpdst.find(curpc)!=jmpdst.end())
	  os << "->";
	else
	  os << "  ";
	HEX(os, curpc) << ":\t";
	os << out.str() << endl;
      }

    const int bytes=pc-oldpc;

    os << "Bytes: ";
    HEX(os, bytes, (bytes<256)?2:4) << "  ";
    os << "Cycles: ";
    HEX(os, cycles, (cycles<256)?2:4) << endl;

    return bytes;
  }

} // namespace MSP430
