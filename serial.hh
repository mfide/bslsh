/* Copyright (c) 2002 Dirk Jagdmann <doj@cubic.org>, Michael Stickel <michael@stickel.org> */

/* This software is provided 'as-is', without any express or implied */
/* warranty. In no event will the authors be held liable for any damages */
/* arising from the use of this software. */

/* Permission is granted to anyone to use this software for any purpose, */
/* including commercial applications, and to alter it and redistribute it */
/* freely, subject to the following restrictions: */

/*     1. The origin of this software must not be misrepresented; you */
/*        must not claim that you wrote the original software. If you use */
/*        this software in a product, an acknowledgment in the product */
/*        documentation would be appreciated but is not required. */

/*     2. Altered source versions must be plainly marked as such, and */
/*        must not be misrepresented as being the original software. */

/*     3. This notice may not be removed or altered from any source */
/*        distribution. */

/* $Header: /code/bslsh/serial.hh,v 1.7 2010/05/22 01:23:26 doj Exp $ */

#ifndef SERIAL__HH
#define SERIAL__HH

#include <string>

#include "tools.hh"

namespace MSP430 {

  namespace BSL {

    class Serial {

    protected:
      int fh;

    public:
      Serial(const std::string& device="/dev/ttyS0") :
	fh(-1)
      {
	connect(device);
      }

      ~Serial()
      {
	disconnect();
      }

      bool connect(const std::string& device);
      bool connected() const;

      bool disconnect();

      bool reset(bool invokeBSL=false);

      bool transmit(const BYTE *frame, const size_t frameLen);
      bool receive(const BYTE *frame, const size_t frameLen, BYTE *buf);

    protected:
      void delay(const unsigned s) const;

      bool clearBuffer();

      bool read(BYTE *data, size_t& len);

      bool RST(bool level);
      bool TEST(bool level);

      enum {
	CMD_FAILED = 0x70,
	SYNC = 0x80,
	DATA_ACK = 0x90,
	DATA_NAK = 0xA0,
      };

    };

  } // namespace BSL

} // namespace MSP430

#endif // SERIAL__HH
