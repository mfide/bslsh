/* Copyright (c) 2002 Dirk Jagdmann <doj@cubic.org>, Michael Stickel <michael@stickel.org> */

/* This software is provided 'as-is', without any express or implied */
/* warranty. In no event will the authors be held liable for any damages */
/* arising from the use of this software. */

/* Permission is granted to anyone to use this software for any purpose, */
/* including commercial applications, and to alter it and redistribute it */
/* freely, subject to the following restrictions: */

/*     1. The origin of this software must not be misrepresented; you */
/*        must not claim that you wrote the original software. If you use */
/*        this software in a product, an acknowledgment in the product */
/*        documentation would be appreciated but is not required. */

/*     2. Altered source versions must be plainly marked as such, and */
/*        must not be misrepresented as being the original software. */

/*     3. This notice may not be removed or altered from any source */
/*        distribution. */

/* $Header: /code/bslsh/command.cc,v 1.9 2010/05/22 01:23:25 doj Exp $ */

#include <iostream>
#include "command.hh"
using namespace std;

namespace MSP430 {

  namespace BSL {

      bool Command::setPassword(BYTE *data)
      {
	VERBOSE("Command::setPassword()\n");
	if(data==NULL)
	  {
	    cerr << "Command::setPassword(): data==NULL" << endl;
	    return false;
	  }
	memcpy(pw, data, 32);
	return true;
      }

      bool Command::poke(const WORD addr, const BYTE b)
      {
	WORD w;
	if(!deek(addr&0xFFFE, w))
	  return false;
	if(addr&1)
	  {
	    w&=0xff;
	    w|=b<<8;
	  }
	else
	  {
	    w&=0xff00;
	    w|=b;
	  }
	return doke(addr&0xFFFE, w);
      }

      bool Command::peek(const WORD addr, BYTE& b)
      {
	WORD w;
	if(!deek(addr&0xFFFE, w))
	  return false;
	if(addr&1)
	  b=w>>8;
	else
	  b=w&0xff;
	return true;
      }

    bool Command::write(const WORD addr, const WORD len, const BYTE *data, const bool needpatch)
    {
      VERBOSE("Command::write()\n");

      if(data==NULL)
	{
	  cerr << "Command::write(): data==NULL" << endl;
	  return false;
	}

      if(addr+len>0x10000)
	{
	  cerr << "Command::write(): addr+len>0x10000" << endl;
	  return false;
	}

      // the BSL loader does not like to write across memory
      // boundaries aligned at 256 bytes. We therefore split the write
      // request into small writes, which don't cross those
      // boundaries.

      int nextsegment=(addr&0xFF00)+0x100;
      int l;
      for(int i=0; i<len; i+=l)
	{
	  const WORD adr=addr+i;

	  l=len-i;

	  if(l>250)
	    l=250;

	  if(adr+l>nextsegment)
	    {
	      l=nextsegment-adr;
	      nextsegment+=0x100;
	    }

	  if(needpatch)
	    if(!patch())
	      {
		VERBOSE("could not patch\n");
		return false;
	      }

	  if(!bc.transmit(&data[i], l, adr))
	    return false;
	}

      return true;
    }

    bool Command::read(const WORD addr, const WORD len, BYTE *data)
    {
      VERBOSE("Command::read()\n");

      if(data==NULL)
	{
	  cerr << "Command::read(): data==NULL" << endl;
	  return false;
	}

      if((long)addr+(long)len > 0x10000)
	{
	  cerr << "Command::read(): addr+len>0x10000" << endl;
	  return false;
	}

      int maxlen=len;
      if(maxlen>250)
	maxlen=250;

      int l=len;
      for(int i=0; i<len; i+=maxlen, l-=maxlen)
	if(!bc.receive(addr+i, (l>maxlen)?maxlen:l, &data[i]))
	  return false;

      return true;
    }

    bool Command::patch()
    {
      VERBOSE("Command::patch()\n");

      if(BSLver==0)
	if(!readversion())
	  {
	    VERBOSE("could not readversion()\n");
	    return false;
	  }

      if(BSLver>0x0110)
	{
	  VERBOSE("BSL version does not need patching\n");
	  return true;
	}

      const BYTE data[] =
	{
	  0x31, 0x40, 0x1A, 0x02, 0x09, 0x43, 0xB0, 0x12, 0x2A, 0x0E, 0xB0, 0x12, 0xBA, 0x0D, 0x55, 0x42,
	  0x0B, 0x02, 0x75, 0x90, 0x12, 0x00, 0x1F, 0x24, 0xB0, 0x12, 0xBA, 0x02, 0x55, 0x42, 0x0B, 0x02,
	  0x75, 0x90, 0x16, 0x00, 0x16, 0x24, 0x75, 0x90, 0x14, 0x00, 0x11, 0x24, 0xB0, 0x12, 0x84, 0x0E,
	  0x06, 0x3C, 0xB0, 0x12, 0x94, 0x0E, 0x03, 0x3C, 0x21, 0x53, 0xB0, 0x12, 0x8C, 0x0E, 0xB2, 0x40,
	  0x10, 0xA5, 0x2C, 0x01, 0xB2, 0x40, 0x00, 0xA5, 0x28, 0x01, 0x30, 0x40, 0x42, 0x0C, 0x30, 0x40,
	  0x76, 0x0D, 0x30, 0x40, 0xAC, 0x0C, 0x16, 0x42, 0x0E, 0x02, 0x17, 0x42, 0x10, 0x02, 0xE2, 0xB2,
	  0x08, 0x02, 0x14, 0x24, 0xB0, 0x12, 0x10, 0x0F, 0x36, 0x90, 0x00, 0x10, 0x06, 0x28, 0xB2, 0x40,
	  0x00, 0xA5, 0x2C, 0x01, 0xB2, 0x40, 0x40, 0xA5, 0x28, 0x01, 0xD6, 0x42, 0x06, 0x02, 0x00, 0x00,
	  0x16, 0x53, 0x17, 0x83, 0xEF, 0x23, 0xB0, 0x12, 0xBA, 0x02, 0xD3, 0x3F, 0xB0, 0x12, 0x10, 0x0F,
	  0x17, 0x83, 0xFC, 0x23, 0xB0, 0x12, 0xBA, 0x02, 0xD0, 0x3F, 0x18, 0x42, 0x12, 0x02, 0xB0, 0x12,
	  0x10, 0x0F, 0xD2, 0x42, 0x06, 0x02, 0x12, 0x02, 0xB0, 0x12, 0x10, 0x0F, 0xD2, 0x42, 0x06, 0x02,
	  0x13, 0x02, 0x38, 0xE3, 0x18, 0x92, 0x12, 0x02, 0xBF, 0x23, 0xE2, 0xB3, 0x08, 0x02, 0xBC, 0x23,
	  0x30, 0x41
	};

      if(!patchuploaded)
	if(!write(0x220, sizeof(data), data, false))
	  return false;

      patchuploaded=true;
      return go(0x220);
    }

    bool Command::startBSL()
    {
      VERBOSE("Command::startBSL()\n");
      if(!bc.reset(true))
	return false;

      if(!sendPassword())
	{
	  cout << "could not set default password" << endl;
	  return true;
	}

      return readversion();
    }

    bool Command::readversion()
    {
      VERBOSE("Command::readversion()\n");

      if(MSPver && BSLver)
	{
	  VERBOSE("version cached\n");
	  return true;
	}

      BYTE mspLo, mspHi, bslLo, bslHi;

      if(!peek(0xff0, mspHi))
	{
	  cerr << "could not peek" << endl;
	  return false;
	}
      if(!peek(0xff1, mspLo))
	{
	  cerr << "could not peek" << endl;
	  return false;
	}

      MSPver=(mspHi<<8)|mspLo;

      cout << "MSP430 device: ";
      HEX(cout, MSPver) << endl;

      if(!peek(0xffa, bslHi))
	{
	  cerr << "could not peek" << endl;
	  return false;
	}
      if(!peek(0xffb, bslLo))
	{
	  cerr << "could not peek" << endl;
	  return false;
	}

      BSLver=(bslHi<<8)|bslLo;

      cout << "BSL version: ";
      HEX(cout, bslHi, 2) << ".";
      HEX(cout, bslLo, 2) << endl;

      return true;
    }

  }

}
