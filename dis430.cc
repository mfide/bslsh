/* Copyright (c) 2002 Dirk Jagdmann <doj@cubic.org>, Michael Stickel <michael@stickel.org> */

/* This software is provided 'as-is', without any express or implied */
/* warranty. In no event will the authors be held liable for any damages */
/* arising from the use of this software. */

/* Permission is granted to anyone to use this software for any purpose, */
/* including commercial applications, and to alter it and redistribute it */
/* freely, subject to the following restrictions: */

/*     1. The origin of this software must not be misrepresented; you */
/*        must not claim that you wrote the original software. If you use */
/*        this software in a product, an acknowledgment in the product */
/*        documentation would be appreciated but is not required. */

/*     2. Altered source versions must be plainly marked as such, and */
/*        must not be misrepresented as being the original software. */

/*     3. This notice may not be removed or altered from any source */
/*        distribution. */

/* $Header: /code/bslsh/dis430.cc,v 1.4 2010/05/22 01:23:26 doj Exp $ */

#include <cstdlib>
#include <cstdio>
#include <sstream>
#include "disassembler.hh"

using namespace std;

int main(int argc, char **argv)
{
  if(argc<2)
    {
      cerr << "dis430 <filename> [from] [to]" << endl;
      cerr << "from and to in hex" << endl;
      return 1;
    }

  size_t from=0;
  size_t to=0xff;

  if(argc>=3)
    from=strtol(argv[2], NULL, 16);
  if(from<0 || from>0xFFFF)
    {
      cerr << "from parameter not valid" << endl;
      return 1;
    }

  if(argc>=4)
    to=strtol(argv[3], NULL, 16);
  if(to<0 || to>0xFFFF)
    {
      cerr << "to parameter not valid" << endl;
      return 1;
    }

  if(to<=from)
    {
      cerr << hex; // << uppercase;
      cerr << "to:" << to << " <= from:" << from << endl;
      return 1;
    }

  FILE *f=fopen(argv[1], "r");
  if(fseek(f, 0, SEEK_END) < 0)
    {
      cerr << "could not seek 1" << endl;
      return 1;
    }

  size_t size=ftell(f);

  if(size<0)
    return 1;

  if(from>size)
    {
      cerr << "file size " << size << " is too small to start disassembling at " << from << endl;
      return 1;
    }

  if(to>size)
    to=size;

  if(fseek(f, 0, SEEK_SET) < 0)
    {
      cerr << "could not seek 2" << endl;
      return 1;
    }

  BYTE *mem=new BYTE[size+16];
  if(mem==NULL)
    {
      cerr << "could not alloc" << endl;
      return 1;
    }

  if(fread(mem, 1, size, f) != size)
    {
      cerr << "could not read" << endl;
      return 1;
    }

  if(fclose(f) < 0)
    cerr << "could not close" << endl;

  // first disassemble run to get all jump destinations
  ostringstream nil;
  MSP430::Disassembler d(mem);
  d.disassemble(to-from, from, nil);

  // now output
  d.disassemble(to-from, from);

  delete [] mem;

  return 0;
}
