/* Copyright (c) 2002..2012 Dirk Jagdmann <doj@cubic.org>, Michael Stickel <michael@stickel.org> */

/* This software is provided 'as-is', without any express or implied */
/* warranty. In no event will the authors be held liable for any damages */
/* arising from the use of this software. */

/* Permission is granted to anyone to use this software for any purpose, */
/* including commercial applications, and to alter it and redistribute it */
/* freely, subject to the following restrictions: */

/*     1. The origin of this software must not be misrepresented; you */
/*        must not claim that you wrote the original software. If you use */
/*        this software in a product, an acknowledgment in the product */
/*        documentation would be appreciated but is not required. */

/*     2. Altered source versions must be plainly marked as such, and */
/*        must not be misrepresented as being the original software. */

/*     3. This notice may not be removed or altered from any source */
/*        distribution. */

/* $Header: /code/bslsh/shell.cc,v 1.36 2012/06/26 04:31:25 doj Exp $ */

/* shell.c
 *
 * This file is the shell that uses the functionality provided
 * by bsl.cc to access the MSP430 target thru the BSL-Port and
 * provide these functions to the user either as command line
 * options or interactively.
 *
 */

#include <srecord/input/file/intel.h>
#include <srecord/memory.h>
#include <srecord/record.h>
#include <srecord/defcon.h>


#include <cstdio>
#include <cctype>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

#ifdef USE_READLINE
#include <readline/readline.h>
#include <readline/history.h>
#endif

#include "command.hh"
#include "debug.h"
#include "disassembler.hh"
#include "registername.hh"

using namespace std;
using namespace MSP430::BSL;

/*---------------------------------------------------------------
 * Global Variables:
 *---------------------------------------------------------------*/

static const char *programName    = "MSP430 Bootstrap Loader Shell";
static const char *programVersion = "Version 2.6";
#ifdef USE_READLINE
static const char *historyFile    = "~/.bslsh";
#endif

bool be_quiet = false;
bool be_verbose = false;
#ifdef DOJDEBUG
int debug_level = 0;
#endif

static Command BSL;

typedef struct
{
  const char *name;
  int (*command) (istringstream&);

  const char *synopsys;  // how to invoke
  const char *shortHelp;
  const char *longHelp;
} ShellCommand_t;

ShellCommand_t* findCommand(const string&);
int commandHelp(istringstream& is);


/**
 * Display a help for a specific command.
 */
int showHelpFor(const string& name)
{
  ShellCommand_t *cmd = findCommand(name);
  if (cmd)
    {
      cout << "Name:\t\t" << cmd->name << endl;
      cout << "Synopsys:\t" << cmd->name << " " << cmd->synopsys << endl;
      cout << "Description:\t" << cmd->shortHelp << endl;
      if (cmd->longHelp)
	cout << cmd->longHelp << endl;
      return 0;
    }
  return -1;
}


/**
 *
 *
 *
 */
int commandPoke (istringstream& is)
{
  string a,v;
  is >> a >> v;
  if (a.size() && v.size())
    {
      const int addr   = strtoul(a.c_str(), NULL, 0);
      const BYTE value = strtoul(v.c_str(), NULL, 0);

      if(BSL.poke(addr, value))
	cout << "OK" << endl;
      else
	cerr << "Error while pokeing" << endl;

      return 0;
    }
  return -1;
}

int commandDoke (istringstream& is)
{
  string a,v;
  is >> a >> v;
  if (a.size() && v.size())
    {
      const int addr   = strtoul(a.c_str(), NULL, 0);
      const WORD value = strtoul(v.c_str(), NULL, 0);

      if(BSL.doke(addr, value))
	cout << "OK" << endl;
      else
	cerr << "Error while dokeing" << endl;

      return 0;
    }
  return -1;
}

/**
 *
 *
 */
int commandPeek (istringstream& is)
{
  string a;
  is >> a;
  if (a.size())
    {
      const int addr  = strtoul(a.c_str(), NULL, 0);
      BYTE b;
      if(!BSL.peek(addr, b))
	cerr << "Error while peeking" << endl;
      else
	{
	  HEX(cout, addr) << ".b = ";
	  HEX(cout, b, 2) << endl;
	}
      return 0;
    }
  return -1;
}


/**
 *
 *
 *
 */
int commandDeek (istringstream& is)
{
  string a;
  is >> a;
  if (a.size())
    {
      const int addr  = strtoul(a.c_str(), NULL, 0);
      WORD w;
      if(!BSL.deek(addr, w))
	cerr << "Error while deeking" << endl;
      else
	{
	  HEX(cout, addr) << ".w = ";
	  HEX(cout, w) << endl;
	}
      return 0;
    }
  return -1;
}

/**
 *
 *
 */
int commandDisassemble (istringstream& is)
{
  // initialize the static Disassembler
  static MSP430::Disassembler *d=NULL;
  if(d==NULL)
    {
      d=new MSP430::Disassembler;
      if(d==NULL)
	{
	  cerr << "could not create new Disassembler" << endl;
	  return 0;
	}
    }
  if(d->mem==NULL)
    {
      d->mem=new BYTE[0x10000+16];
      if(d->mem==NULL)
	{
	  cerr << "could not allocate memory for Disassembler" << endl;
	  return 0;
	}
    }

  // get arguments
  string a, s;
  is >> a >> s;

  // reset if requested
  if(a=="reset")
    {
      d->reset();
      cout << "Disassembler reset" << endl;
      return 0;
    }

  // disassemble some block of memory
  if (a.size())
    {
      const int addr = strtoul(a.c_str(), NULL, 0);

      int size=16;
      if(s.size())
	{
	  if(s=="-")
	    {
	      is >> s;
	      if(s.empty())
		return -1;
	      size = strtoul(s.c_str(), NULL, 0);
	      if(size<=addr)
		{
		  cerr << "to parameter < from parameter" << endl;
		  return -1;
		}
	      size-=addr;
	    }
	  else
	    size = strtoul(s.c_str(), NULL, 0);

	  if(size==0)
	    {
	      LOG("size is 0 bytes. aborting...\n");
	      return -1;
	    }
	}
      VERBOSE("disassemble 0x%04X .. 0x%04X\n", addr, addr+size-1);

      if(BSL.read(addr, size, &(d->mem[addr])))
	{
	  string file;
	  is >> file;
	  if(file.size())
	    {
	      ofstream of(file.c_str());
	      if(!of)
		{
		  cerr << "could not open " << file << endl;
		  return 0;
		}
	      cout << "Disassembling to " << file << endl;
	      d->disassemble(size, addr, of);
	    }
	  else
	    d->disassemble(size, addr, cout);
	}

      return 0;
    }
  return -1;
}


static void streamdump(const BYTE *block, const WORD addr, const int size, ostream& os)
{
  for(int i=0; i<size; i+=16)
    {
      HEX(os, addr+i) << ": ";
      for(int j=i; j<i+16; j++)
	{
	  HEX(os, block[j], 2) << " ";
	  if(j==i+7)
	    os << "  ";
	}
      for(int j=i; j<i+16; j++)
	{
	  os << static_cast<char>(isprint(block[j])?block[j]:'.');
	  if(j==i+7)
	    os << "  ";
	}
      os << endl;
    }
}
/**
 *
 *
 */
int commandDump (istringstream& is)
{
  string a, s;
  is >> a >> s;
  if (a.size())
    {
      const int addr = strtoul(a.c_str(), NULL, 0);

      int size=16;
      if(s.size())
	{
	  if(s=="-")
	    {
	      is >> s;
	      if(s.empty())
		return -1;
	      size = strtoul(s.c_str(), NULL, 0);
	      if(size<=addr)
		{
		  cerr << "to parameter < from parameter" << endl;
		  return -1;
		}
	      size-=addr;
	    }
	  else
	    size = strtoul(s.c_str(), NULL, 0);

	  if(size==0)
	    {
	      LOG("size is 0 bytes. aborting...\n");
	      return -1;
	    }
	}
      size+=size%16;		// round to 16byte
      if(size<16)
	size=16;

      VERBOSE("dump 0x%04X .. 0x%04X\n", addr, addr+size-1);

      BYTE *block = new BYTE[size];
      if(block==NULL)
	{
	  cerr << "could not allocate memory" << endl;
	  return 0;
	}

      if(BSL.read(addr, size, block))
	{
	  string file;
	  is >> file;
	  if(file.size())
	    {
	      ofstream of(file.c_str());
	      if(!of)
		{
		  cerr << "could not open " << file << endl;
		  goto dumperror;
		}
	      cout << "Dumping to " << file << endl;
	      streamdump(block, addr, size, of);
	    }
	  else
	    streamdump(block, addr, size, cout);
	}
      else
	cerr << "could not read" << endl;

    dumperror:
      delete [] block;
      return 0;
    }
  return -1;
}


/**
 *
 *
 *
 */
int commandCall (istringstream& is)
{
#if 1
  cerr << "not yet" << endl;
  return 0;
#else
  string a;
  is >> a;
  if (a.size())
    {
      const int addr = strtoul(a.c_str(), NULL, 0);
      cout << "calling code at ";
      HEX(cout, addr) << endl;
      BSL.call(addr);
      return 0;
    }
  return -1;
#endif
}

int commandRun (istringstream& is)
{
  string a;
  is >> a;
  if (a.size())
    {
      WORD addr=0;
      if(a=="rv")
	BSL.doke(0xfffe, addr);
      else
	addr = strtoul(a.c_str(), NULL, 0);
      cout << "BR &";
      HEX(cout, addr) << endl;
      if(!BSL.go(addr))
	return 1;
      return 0;
    }
  return -1;
}


/**
 *
 *
 *
 */
int commandErase (istringstream& is)
{
  string a;
  is >> a;
  if(a.empty())
    return -1;

  if(a=="all")
    {
      is >> a;
      if(a!="--force")
	{
	  cout << "If you are really sure to erase all flash ram, call this command as \"eras all --force\"" << endl;
	  return 0;
	}

      cout << "Mass Erase..." << endl;
      if(BSL.eraseAll())
	cout << "...done" << endl;
      else
	cout << "...error" << endl;

      BYTE pw[32];
      memset(pw, 0xff, sizeof(pw));
      BSL.setPassword(pw);
      BSL.sendPassword();

      return 0;
    }

  const int addr = strtoul(a.c_str(), NULL, 0);

  if(addr&0x7F)
    {
      cout << "addr must be aligned on a 128 byte boundary. aborting..." << endl;
      return 0;
    }

  VERBOSE("erase segment 0x%04X .. 0x%04X\n", addr, addr+0x80);
  if(BSL.eraseSegment(addr))
    cout << "OK" << endl;
  else
    cerr << "could not erase segment" << endl;
  
  return 0;
}


int commandPassword(istringstream& is)
{
  string filename;
  is >> filename;
  if(!filename.size())
    {
      cerr << "No filename given" << endl;
      return -1;
    }

  struct stat buf;
  if(stat(filename.c_str(), &buf) < 0)
    {
      perror("could not stat file. file not present in current work directory?");
      return 0;
    }

  if(S_ISREG(buf.st_rdev))
    {
      cerr << filename << " is no regular file" << endl;
      return 0;
    }

  int ret=0;

  srecord::memory *mp = new srecord::memory();
  if (mp)
    {
      srecord::input::pointer ifp (srecord::input_file_intel::create(filename));
      if(ifp)
	{
	  mp->reader(ifp, srecord::defcon_warning, srecord::defcon_fatal_error);
	  cout << "Using password: ";
	  BYTE pw[32];
	  for (int i=0; i<32; i++)
	    {
	      pw[i] = mp->get(0xffe0+i);
	      HEX(cout, pw[i], 2) << " ";
	    }
	  cout << endl;

	  if(!BSL.setPassword(pw))
	    {
	      cerr << "could not set password" << endl;
	      ret=-1;
	    }

	  BSL.sendPassword();
	}
      else
	{
	  cerr << "could not create ifp" << endl;
	  ret=-1;
	}
      delete mp;
    }
  else
    {
      cerr << "could not create srec_memory" << endl;
      ret=-1;
    }
  
  return ret;
}

/**
 * Reset the target and if the extra parameter "bsl" is
 * given, invoke the bootstrap loader.
 *
 */
int commandReset (istringstream& is)
{
  string s;
  is >> s;
  if (s=="bsl")
    {
      printf ("reseting target and invokeing bootstrap loader\n");
      if(BSL.startBSL())
	BSL.sendPassword();
    }
  else
    BSL.reset();
  return 0;
}


/**
 * The command for exiting the application.
 */
int commandExit (istringstream& is)
{
  exit(0);
}


/**
 *
 *
 *
 */
class flashWalker : public srecord::memory_walker
{
public:
  typedef boost::shared_ptr<flashWalker> pointer;

  static bool swap;

  virtual void observe(unsigned long addr, const void *_data, int size)
  {
    if(size&1)
      LOG("flashWalker::observe(): oops, size is odd\n");

    cout << "flashing ";
    HEX(cout, addr) << "...";
    HEX(cout, addr+size-1) << " ";

    const BYTE *data=reinterpret_cast<const BYTE*>(_data);

    if(swap)
      {
	BYTE *buf=new BYTE[size+1];
	if(buf==NULL)
	  {
	    cerr << "flashWalker::observe(): could not alloc" << endl;
	    return;
	  }

	// swap all bytes, as the BSL expects them in LE and srecord returns them in BE
	for(int i=0; i<size; i+=2)
	  {
	    buf[i]=data[i+1];
	    buf[i+1]=data[i];
	  }

	if(BSL.write(addr, size, buf))
	  cout << "OK" << endl;
	else
	  cout << "ERROR!" << endl;

	delete [] buf;
      }
    else
      {
	if(BSL.write(addr, size, data))
	  cout << "OK" << endl;
	else
	  cout << "ERROR!" << endl;
      }
  }
};

bool flashWalker::swap=false;

int commandFile (istringstream& is)
{
  int ret=0;

  string s;
  is >> s;
  if (s.size())
    {
      size_t i=s.rfind(".hex");
      if(i==string::npos)
	{
	  cerr << s << " is not a hex file. aborting..." << endl;
	  return 0;
	}

      string swp;
      is >> swp;
      if(swp.size() && swp == "swap")
	flashWalker::swap=true;

      struct stat buf;
      if(stat(s.c_str(), &buf) < 0)
	{
	  cerr << "could not open " << s << endl;
	  return 0;
	}

      if(S_ISREG(buf.st_rdev))
	{
	  cerr << s << " is no regular file" << endl;
	  return 0;
	}

      srecord::memory mp;
      srecord::input::pointer ifp(srecord::input_file_intel::create(s));
      if(ifp)
	{
	  mp.reader(ifp, srecord::defcon_warning, srecord::defcon_fatal_error);
	  {
	    // \todo check if this is really want we want as taddr2
	    srecord::record *sr=mp.get_execution_start_address();
	    if(sr)
	      {
		srecord::record::address_t taddr2=sr->get_address();
		cout << "flash start addr: ";
		HEX(cout, taddr2) << endl;
	      }
	  }

	  flashWalker::pointer fw(new flashWalker);
	  mp.walk(fw);

	  // get and set password
	  BYTE pw[32];
	  bool flag=false;
	  for (int i=0; i<32; i++)
	    {
	      pw[i] = mp.get(0xffe0+i);
	      if(pw[i]!=0)
		flag=true;
	    }

	  if(flag && !BSL.setPassword(pw))
	    {
	      cerr << "could not set password" << endl;
	      ret=-1;
	    }
	}
      else
	{
	  cerr << "could not alloc srec_input_file" << endl;
	  ret=-1;
	}
    }

  return ret;
}

int commandVerbose(istringstream& is)
{
  string a;
  is >> a;
  if(a=="on")
    be_verbose=true;
  else if(a=="off")
    be_verbose=false;
  else
    return -1;
  return 0;
}

int commandQuiet(istringstream& is)
{
  string a;
  is >> a;
  if(a=="on")
    be_quiet=true;
  else if(a=="off")
    be_quiet=false;
  else
    return -1;
  return 0;
}

int commandFill(istringstream& is)
{
  string a;
  is >> a;
  if(a.empty())
    return -1;
  const int addr =  strtoul(a.c_str(), NULL, 0);

  string s;
  is >> s;
  if(s.empty())
    return -1;

  int size=1;
  if(s=="-")
    {
      is >> s;
      if(s.empty())
	return -1;
      size = strtoul(s.c_str(), NULL, 0);
      if(size<=addr)
	{
	  cerr << "to parameter < from parameter" << endl;
	  return -1;
	}
      size-=addr;
    }
  else
    size = strtoul(s.c_str(), NULL, 0);

  string b;
  is >> b;
  if(b.empty())
    return -1;
  const BYTE byte = strtoul(b.c_str(), NULL, 0);

  if(addr<0 || addr>0xFFFF)
    {
      cerr << "address out of range. aborting..." << endl;
      return -1;
    }
  if(size<0 || size>0xFFFF)
    {
      cerr << "length out of range. aborting..." << endl;
      return -1;
    }
  if(addr+size>0x10000)
    {
      cerr << "address and length exceed 0xFFFF. aborting..." << endl;
      return -1;
    }

  BYTE *data=new BYTE[size];
  if(data==NULL)
    {
      cerr << "could not alloc. aborting..." << endl;
      return 0;
    }
  memset(data, byte, size);
  
  if(BSL.write(addr, size, data))
    cout << "OK" << endl;
  else
    cout << "Error" << endl;
  
  delete [] data;
  return 0;
}

int commandLs(istringstream& is)
{
  return system(is.str().c_str());
}


string execute(const string& command)
{
  string s;
  FILE *f=popen(command.c_str(), "r");
  if(f==NULL)
    {
      perror("could not execute command");
      return s;
    }
  char buf[1024];
  while(fgets(buf, sizeof(buf), f))
    s+=buf;
  if(pclose(f) < 0)
    perror("could not close pipe");
  return s;
}

int commandCd(istringstream& is)
{
  string p;
  is >> p;
  if(p.size())
    return chdir(execute(string("echo -n ")+p).c_str());
  return -1;
}


/**
 *
 *
 */
int ExecuteBSLCommand (const string& input)
{
  if(input.empty())
    return -1;

  istringstream is(input);
  string command;
  is >> command;

  if(command.empty())
    return 0;

  // check for comment
  if(command[0]=='#')
    return 0;
  if(command[0]==';')
    return 0;
  if(command[0]=='%')
    return 0;
  if(command[0]=='/' && command[1]=='/')
    return 0;

  // check for shell escape
  if(command[0]=='!')
    {
      size_t pos=input.find('!');
      if(pos==string::npos)	// should not happen
	{
	  cerr << "expected to find '!' somewhere. aborting..." << endl;
	  return 0;
	}
      string s(input, pos+1);
      cout << execute(s) << endl;
      return 0;
    }

  // find bsl shell command
  ShellCommand_t *cmd = findCommand(command);
  if(cmd==NULL)
    {
      cerr << "unrecognized command" << endl;
      return -1;
    }

  // execute it
  const int r=cmd->command(is);
  // print help or errno string if command returned an error
  if(r<0)
    {
      showHelpFor(command);
    }
  return r;
}


#if 0
int ExecuteBSLCommandFile (const string& filename)
{
  ifstream is;
  is.open(filename.c_str());
  if(!is)
    return -1;
  int z=1;
  while(is)
    {
      string line;
      getline(is, line);
      if(line.size())
	if(ExecuteBSLCommand(line) < 0)
	  {
	    cerr << filename << ":" << z << ": error in command file" << endl;
	    return -1;
	  }
      z++;
    }
  return 0;
}
#endif


int commandStatus(istringstream& is)
{
  cout << "Status" << endl;
  return 0;
}

int commandVersion(istringstream& is)
{
  cout << "MSP Version: ";
  HEX(cout, BSL.MSPversion()) << endl;

  cout << "BSL Version: ";
  HEX(cout, BSL.BSLversion()) << endl;

  return 0;
}


/*
 * This is a list of command available to the shell.
 * If you whant to add commands do it here or never.
 */
ShellCommand_t commands[] =
{
  {"help",      commandHelp,        "[<command>]",     "Shows help", NULL},
  {"?",         commandHelp,        "[<command>]",     "Shows help", NULL},
  {"call",      commandCall,        "<addr>",          "Call a function at <addr>", NULL},
  {"cd",        commandCd,          "<dir>",           "change current directory to dir", NULL},
  {"deek",      commandDeek,        "<addr>",          "Reads a word from address <addr>", NULL},
  {"disas",     commandDisassemble, "<range> [<file>]","Disassembles the memory in <range>, default size 16", NULL},
  {"doke",      commandDoke,        "<addr> <word>",   "Write a word at address <word>", NULL},
  {"dump",      commandDump,        "<range> [<file>]","Dumps memory in <range>, default size 16", NULL},
  {"erase",     commandErase,       "<addr> | \"all\"","Erase a flash segment (handle with care!)", NULL},
  {"fill",      commandFill,        "<range> <byte>",  "fill memory block with byte, default size 1", NULL},
  {"flash",     commandFile,        "<filename> [\"swap\"]","flash <filename> into target", NULL},
  {"go",        commandRun,         "<addr>|\"rv\"",   "Execute code at <addr> or standard reset vector", NULL},
  {"ls",        commandLs,          "[<arg>*]",        "execute the ls program", NULL},
  {"password",  commandPassword,    "<file>",          "read password from file and transmit to target", NULL},
  {"peek",      commandPeek,        "<addr>",          "Reads a byte from address <addr>", NULL},
  {"poke",      commandPoke,        "<addr> <byte>",   "Writes a byte at the address <addr>", NULL},
  {"quiet",     commandVerbose,     "\"on\" | \"off\"","enable quiet mode", NULL},
  {"quit",      commandExit,        "",                "Exit this application", NULL},
  {"q",         commandExit,        "",                "Exit this application", NULL},
  {"reset",     commandReset,       "[\"bsl\"]",       "Resets the target", NULL},
  {"run",       commandRun,         "<addr>|\"rv\"",   "Execute code at <addr> or standard reset vector", NULL},
  {"status",    commandStatus,      "",                "Print status information", NULL},
  {"verbose",   commandVerbose,     "\"on\" | \"off\"","enable verbose mode", NULL},
  {"version",   commandVersion,     "",                "print version", NULL},
};

/**
 * This is the help command. It displays a list of all commands
 * or a help to a specific command if it is given and it exists.
 *
 */
int commandHelp(istringstream& is)
{
  string name;
  is >> name;
  if(name.size() && showHelpFor(name)==0)
    return 0;

  for (unsigned int i=0; i < sizeof(commands)/sizeof(commands[0]); i++)
    {
      ShellCommand_t *cmd = &commands[i];
      printf ("%-9s%-20s%s\n", cmd->name, cmd->synopsys, cmd->shortHelp);
    }

  printf("a <range> can have the following form:\n"
	 "<start addr>  with a default size specific to the command.\n"
	 "<start addr> <size>\n"
	 "<start addr> - <end addr>\n");

  return 0;
}


/**
 * Find a command by name.
 *
 * @parm   name  Name of the command
 * @return A pointer to the Command or NULL if command has not been found.
 */
ShellCommand_t *findCommand (const string& name)
{
  for (unsigned int i=0; i < sizeof(commands)/sizeof(commands[0]); i++)
    if (name==commands[i].name)
      return &commands[i];
  return NULL;
}

#ifdef USE_READLINE
void flushHistory()
{
  write_history(historyFile);
  cout << endl;
}
#else
char* readline(const char* prompt)
{
  cout << prompt << flush;
  std::string l;
  getline(cin, l);
  if (l.empty()) return NULL;
  return strdup(l.c_str());
}
#endif

/**
 * Show the help for the command line options.
 */
void showHelp()
{
  cout << endl;
  cout << "usage: bslsh [options]" << endl;
  cout << "--device <dev>|-d <dev> use the serial port <dev> for accessing the target" << endl;
  cout << "--bsl                   start BSL upon startup" << endl;
  cout << "--eraseall              erase complete flash memory" << endl;
  cout << "--flash <file>          flash <file>" << endl;
  cout << "--passwordfile <file>   <file> contains 32 bytes that represents the password" << endl;
  cout << "--go <addr> | \"rv\"      execute at addr or standard reset vector" << endl;
  cout << "--reset                 make a reset" << endl;
#ifdef DOJDEBUG
  cout << "--debug <level>         set debug level to <level>" << endl;
#endif
  cout << "--quiet                 dont say anything except the data output" << endl;
  cout << "--verbose               do some more output" << endl;
  cout << "--help | -h | -?        show this help" << endl;
  cout << endl;
}

/*---------------------------------------------------------------
 * Main:
 *---------------------------------------------------------------
 */
int main (const int argc, const char *argv[])
{
  cout << programName << " " << programVersion << endl;

  string bslshrc=getenv("HOME");
  bslshrc+="/.bslshrc";
  // get device name from previous run
  {
    ifstream is(bslshrc.c_str());
    if(is)
      {
	string d;
	is >> d;
	BSL.open(d);
      }
  }

  bool startbsl=false;
  bool eraseall=false;
  bool reset=false;
  const char *passwordfile=NULL;
  const char *go=NULL;
  const char *flashfile=NULL;

  for (int i=1; i<argc; i++)
    {
      if (!strcmp(argv[i], "--device") || !strcmp(argv[i], "-d"))
	{
	  ++i;
	  VERBOSE("using serial %s\n", argv[i]);
	  if(!BSL.open(argv[i]))
	    {
	      cerr << "could not open " << argv[i] << endl;
	      return 1;
	    }
	  // save device name
	  ofstream os(bslshrc.c_str());
	  if(os)
	    os << argv[i] << endl;
	}
      else if (!strcmp(argv[i], "--help") || !strcmp(argv[i], "-h") || !strcmp(argv[i], "-?"))
	{
	  showHelp();
	  return(0);
	}
      else if (!strcmp(argv[i], "--quiet"))
	{ 
	  be_verbose=false; be_quiet=true;
	}
      else if (!strcmp(argv[i], "--verbose"))
	{ 
	  if (!be_quiet) 
	    be_verbose=true; 
	}
      else if (!strcmp(argv[i], "--passwordfile"))
	passwordfile=argv[++i];
      else if (!strcmp(argv[i], "--flash"))
	flashfile=argv[++i];
      else if (!strcmp(argv[i], "--bsl"))
	startbsl=true;
      else if (!strcmp(argv[i], "--eraseall"))
	eraseall=true;
      else if (!strcmp(argv[i], "--reset"))
	reset=true;
      else if (!strcmp(argv[i], "--go"))
	go=argv[++i];
#ifdef DOJDEBUG
      else if (!strcmp(argv[i], "--debug")) debug_level=atoi(argv[++i]);
#endif
      else
	{
	  printf ("unknown option \"%s\"\n", argv[i]);
	  return 1;
	}
    }

  // setenv("INPUTRC", "~/bslshrc"); // override default inputrc

  if(!BSL.connected())
    {
      cerr << "not connected to any serial port. perhaps you should use the --device option?" << endl;
      return 1;
    }

  if(startbsl)
    {
      if(!BSL.startBSL())
	{
	  cerr << "could not start BSL" << endl;
	  return 1;
	}
    }

  if(passwordfile)
    {
      istringstream is(passwordfile);
      commandPassword(is);
    }

  if(eraseall)
    {
      istringstream is("all --force");
      commandErase(is);
    }

  if(flashfile)
    {
      istringstream is(flashfile);
      commandFile(is);
    }

  if(go)
    {
      istringstream is(go);
      if(commandRun(is)>0)
	{
	  istringstream is(go);
	  commandRun(is);
	}
    }

  if(reset)
    if(!BSL.reset())
      {
	cerr << "could not reset" << endl;
	return 1;
      }

  /*
   * Main loop of the shell.
   */
#ifdef USE_READLINE
  using_history();
  read_history(historyFile);
  atexit(flushHistory);
#endif
  while (1)
    {
      char prompt[1024];
      strcpy(prompt, "bslsh [");
      getcwd(prompt+strlen(prompt), sizeof(prompt)-strlen(prompt));
      strncat(prompt, "] > ", sizeof(prompt)-strlen(prompt));

      char *cmd = readline(prompt);
      if (!cmd)
	return 0;
      if(*cmd)
	{
#ifdef USE_READLINE
	  add_history(cmd);
#endif

	  // convert command line
	  for(size_t i=0; i<strlen(cmd); i++)
	    {
	      cmd[i]=tolower(cmd[i]);
	      if(cmd[i]==',')
		cmd[i]=' ';
	    }

	  string s(cmd);
	  for(int i=0; MSP430::RegisterName[i].name; i++)
	    {
	      const string name(MSP430::RegisterName[i].name);
	      const size_t f=s.find(name);
	      if(f!=string::npos)
		s.replace(f, name.size(), MSP430::RegisterName[i].addr);
	    }

	  ExecuteBSLCommand(s);
	}

      free(cmd);
    }

  return 0;
}
