/* Copyright (c) 2002 Dirk Jagdmann <doj@cubic.org>, Michael Stickel <michael@stickel.org> */

/* This software is provided 'as-is', without any express or implied */
/* warranty. In no event will the authors be held liable for any damages */
/* arising from the use of this software. */

/* Permission is granted to anyone to use this software for any purpose, */
/* including commercial applications, and to alter it and redistribute it */
/* freely, subject to the following restrictions: */

/*     1. The origin of this software must not be misrepresented; you */
/*        must not claim that you wrote the original software. If you use */
/*        this software in a product, an acknowledgment in the product */
/*        documentation would be appreciated but is not required. */

/*     2. Altered source versions must be plainly marked as such, and */
/*        must not be misrepresented as being the original software. */

/*     3. This notice may not be removed or altered from any source */
/*        distribution. */

/* $Header: /code/bslsh/debug.h,v 1.3 2010/05/22 01:23:26 doj Exp $ */

#ifndef DEBUG__H
#define DEBUG__H

extern bool be_quiet;
extern bool be_verbose;
#ifdef DOJDEBUG
extern int debug_level;
#endif

#define LOG(format, a...) if(!be_quiet) fprintf(stderr, format, ## a), fflush(stderr)
#define VERBOSE(format, a...) if(be_verbose) fprintf(stderr, format, ## a), fflush(stderr)

#ifdef DOJDEBUG
#define DO_DEBUG(level, x) \
do { \
  if(debug_level>level) { \
    x ;\
  } \
} while(0)
#else
#define DO_DEBUG(x)
#endif

#define DBG_FUNCTION(x) DO_DEBUG(255,x)

#endif
