/* Copyright (c) 2002 Dirk Jagdmann <doj@cubic.org>, Michael Stickel <michael@stickel.org> */

/* This software is provided 'as-is', without any express or implied */
/* warranty. In no event will the authors be held liable for any damages */
/* arising from the use of this software. */

/* Permission is granted to anyone to use this software for any purpose, */
/* including commercial applications, and to alter it and redistribute it */
/* freely, subject to the following restrictions: */

/*     1. The origin of this software must not be misrepresented; you */
/*        must not claim that you wrote the original software. If you use */
/*        this software in a product, an acknowledgment in the product */
/*        documentation would be appreciated but is not required. */

/*     2. Altered source versions must be plainly marked as such, and */
/*        must not be misrepresented as being the original software. */

/*     3. This notice may not be removed or altered from any source */
/*        distribution. */

/* $Header: /code/bslsh/command.hh,v 1.8 2010/05/22 01:23:26 doj Exp $ */

#ifndef COMMAND__HH
#define COMMAND__HH

#include <cstring>
#include "basiccommand.hh"

namespace MSP430 {

  namespace BSL {

    class Command {
    protected:
      BasicCommand bc;
      BYTE pw[32];
      WORD MSPver, BSLver;
      bool patchuploaded;

    public:
      Command(const std::string& device="/dev/ttyS0") :
	bc(device),
	MSPver(0), BSLver(0),
	patchuploaded(false)
      {
	memset(pw, 0xff, sizeof(pw));
      }

      bool open(const std::string& device)
      {
	return bc.open(device);
      }

      bool connected()
      {
	return bc.connected();
      }

      bool setPassword(BYTE *data);

      bool sendPassword()
      {
	VERBOSE("Command::sendPassword()\n");
	return bc.password(pw) && readversion();
      }

      bool reset()
      {
	VERBOSE("Command::reset()\n");
	return bc.reset(false);
      }

      bool startBSL();

      bool poke(const WORD addr, const BYTE b);
      bool peek(const WORD addr, BYTE& b);

      bool doke(const WORD addr, const WORD w)
      {
	if(!patch())
	  return false;
	return bc.transmit(reinterpret_cast<const BYTE*>(&w), sizeof(WORD), addr);
      }
      bool deek(const WORD addr, WORD& w)
      {
	return bc.receive(addr, sizeof(WORD), reinterpret_cast<BYTE*>(&w));
      }

      bool write(const WORD addr, const WORD len, const BYTE *data, const bool needpatch=true);
      bool read(const WORD addr, const WORD len, BYTE *data);

      bool eraseSegment(const WORD addr)
      {
	patchuploaded=false;
	return bc.eraseSegment(addr);
      }
      bool eraseAll()
      {
	patchuploaded=false;
	return bc.eraseAll() && startBSL() && readversion();
      }

      bool go(const WORD addr)
      {
	return bc.loadpc(addr);
      }
      bool call(const WORD addr);

      bool patch();

      bool readversion();

      WORD MSPversion() const { return MSPver; }
      WORD BSLversion() const { return BSLver; }

    };

  } // namespace BSL

} // namespace MSP430

#endif // COMMAND__HH
