/* Copyright (c) 2002 Dirk Jagdmann <doj@cubic.org>, Michael Stickel <michael@stickel.org> */

/* This software is provided 'as-is', without any express or implied */
/* warranty. In no event will the authors be held liable for any damages */
/* arising from the use of this software. */

/* Permission is granted to anyone to use this software for any purpose, */
/* including commercial applications, and to alter it and redistribute it */
/* freely, subject to the following restrictions: */

/*     1. The origin of this software must not be misrepresented; you */
/*        must not claim that you wrote the original software. If you use */
/*        this software in a product, an acknowledgment in the product */
/*        documentation would be appreciated but is not required. */

/*     2. Altered source versions must be plainly marked as such, and */
/*        must not be misrepresented as being the original software. */

/*     3. This notice may not be removed or altered from any source */
/*        distribution. */

/* $Header: /code/bslsh/registername.cc,v 1.4 2010/05/22 01:23:26 doj Exp $ */

#include "registername.hh"

namespace MSP430 {

struct RegisterName_t RegisterName[] = {
  { "ie1",	"00", 0x00 },
  { "ie2",	"01", 0x01 },
  { "ifg1",	"02", 0x02 },
  { "ifg2",	"03", 0x03 },
  { "me1",	"04", 0x04 },
  { "me2",	"05", 0x05 },
  { "p0in",	"10", 0x10 },
  { "p0out",	"11", 0x11 },
  { "p0dir",	"12", 0x12 },
  { "p0ifg",	"13", 0x13 },
  { "p0ies",	"14", 0x14 },
  { "p0ie",	"15", 0x15 },
  { "p3in",	"18", 0x18 },
  { "p3out",	"19", 0x19 },
  { "p3dir",	"1A", 0x1A },
  { "p3sel",	"1B", 0x1B },
  { "p4in",	"1C", 0x1C },
  { "p4out",	"1D", 0x1D },
  { "p4dir",	"1E", 0x1E },
  { "p4sel",	"1F", 0x1F },
  { "p1in",	"20", 0x20 },
  { "p1out",	"21", 0x21 },
  { "p1dir",	"22", 0x22 },
  { "p1ifg",	"23", 0x23 },
  { "p1ies",	"24", 0x24 },
  { "p1ie",	"25", 0x25 },
  { "p1sel",	"26", 0x26 },
  { "p2in",	"28", 0x28 },
  { "p2out",	"29", 0x29 },
  { "p2dir",	"2A", 0x2A },
  { "p2ifg",	"2B", 0x2B },
  { "p2ies",	"2C", 0x2C },
  { "p2ie",	"2D", 0x2D },
  { "p2sel",	"2E", 0x2E },
  { "p5in",	"30", 0x30 },
  { "p5out",	"31", 0x31 },
  { "p5dir",	"32", 0x32 },
  { "p5sel",	"33", 0x33 },
  { "p6in",	"34", 0x34 },
  { "p6out",	"35", 0x35 },
  { "p6dir",	"36", 0x36 },
  { "p6sel",	"37", 0x37 },
  { "lcdctl",	"30", 0x30 },
  { "lcd_start","31", 0x31 },
  { "lcd_stop",	"3f", 0x3f },
  { "btctl",	"40", 0x40 },
  { "tcctl",	"42", 0x42 },
  { "tcpld",	"43", 0x43 },
  { "tcdat",	"44", 0x44 },
  { "btcnt1",	"46", 0x46 },
  { "btcnt2",	"47", 0x47 },
  { "tpctl",	"4B", 0x4B },
  { "tpcnt1",	"4C", 0x4C },
  { "tpcnt2",	"4D", 0x4D },
  { "tpd",	"4E", 0x4E },
  { "tpe",	"4F", 0x4F },
  { "scfi0",	"50", 0x50 },
  { "scfi1",	"51", 0x51 },
  { "scfqctl",	"52", 0x52 },
  { "cbctl",	"53", 0x53 },
  { "epctl",	"54", 0x54 },
  { "dcoctl",	"56", 0x56 },
  { "bcsctl1",	"57", 0x57 },
  { "bcsctl2",	"58", 0x58 },
  { "cactl1",	"59", 0x59 },
  { "cactl2",	"5A", 0x5A },
  { "capd",	"5B", 0x5B },
  { "u0ctl",	"70", 0x70 },
  { "u0tctl",	"71", 0x71 },
  { "u0rctl",	"72", 0x72 },
  { "u0mctl",	"73", 0x73 },
  { "u0br0",	"74", 0x74 },
  { "u0br1",	"75", 0x75 },
  { "u0rxbuf",	"76", 0x76 },
  { "u0txbuf",	"77", 0x77 },
  { "u1ctl",	"78", 0x78 },
  { "u1tctl",	"79", 0x79 },
  { "u1rctl",	"7A", 0x7A },
  { "u1mctl",	"7B", 0x7B },
  { "u1br0",	"7C", 0x7C },
  { "u1br1",	"7D", 0x7D },
  { "u1rxbuf",	"7E", 0x7E },
  { "u1txbuf",	"7F", 0x7F },
  { "adc12mctl0",	"80", 0x80 },
  { "adc12mctl1",	"81", 0x81 },
  { "adc12mctl2",	"82", 0x82 },
  { "adc12mctl3",	"83", 0x83 },
  { "adc12mctl4",	"84", 0x84 },
  { "adc12mctl5",	"85", 0x85 },
  { "adc12mctl6",	"86", 0x86 },
  { "adc12mctl7",	"87", 0x87 },
  { "adc12mctl8",	"88", 0x88 },
  { "adc12mctl9",	"89", 0x89 },
  { "adc12mctl10",	"8A", 0x8A },
  { "adc12mctl11",	"8B", 0x8B },
  { "adc12mctl12",	"8C", 0x8C },
  { "adc12mctl13",	"8D", 0x8D },
  { "adc12mctl14",	"8E", 0x8E },
  { "adc12mctl15",	"8F", 0x8F },
  { "lcdc",	"90", 0x90 },
  { "ain",	"110", 0x110 },
  { "aen",	"112", 0x112 },
  { "actl",	"114", 0x114 },
  { "adat",	"118", 0x118 },
  { "tbiv",	"11E", 0x11E },
  { "wdtctl",	"120", 0x120 },
  { "taiv",	"12E", 0x12E },
  { "fctl1",	"128", 0x128 },
  { "fctl2",	"12A", 0x12A },
  { "fctl3",	"12C", 0x12C },
  { "mpy",	"130", 0x130 },
  { "mpys",	"132", 0x132 },
  { "mac",	"134", 0x134 },
  { "macs",	"136", 0x136 },
  { "op2",	"138", 0x138 },
  { "reslo",	"13A", 0x13A },
  { "reshi",	"13C", 0x13C },
  { "sumext",	"13E", 0x13E },
  { "adc12mem0",	"140", 0x140 },
  { "adc12mem1",	"142", 0x142 },
  { "adc12mem2",	"144", 0x144 },
  { "adc12mem3",	"146", 0x146 },
  { "adc12mem4",	"148", 0x148 },
  { "adc12mem5",	"14A", 0x14A },
  { "adc12mem6",	"14C", 0x14C },
  { "adc12mem7",	"14E", 0x14E },
  { "adc12mem8",	"150", 0x150 },
  { "adc12mem9",	"152", 0x152 },
  { "adc12mem10",	"154", 0x154 },
  { "adc12mem11",	"156", 0x156 },
  { "adc12mem12",	"158", 0x158 },
  { "adc12mem13",	"15A", 0x15A },
  { "adc12mem14",	"15C", 0x15C },
  { "adc12mem15",	"15E", 0x15E },
  { "tactl",	"160", 0x160 },
  { "cctl0",	"162", 0x162 },
  { "cctl1",	"164", 0x164 },
  { "cctl2",	"166", 0x166 },
  { "cctl3",	"168", 0x168 },
  { "cctl4",	"16A", 0x16A },
  { "tar",	"170", 0x170 },
  { "ccr0",	"172", 0x172 },
  { "ccr1",	"174", 0x174 },
  { "ccr2",	"176", 0x176 },
  { "ccr3",	"178", 0x178 },
  { "ccr4",	"17A", 0x17A },
  { "tbctl",	"180", 0x180 },
  { "tbcctl0",	"182", 0x182 },
  { "tbcctl1",	"184", 0x184 },
  { "tbcctl2",	"186", 0x186 },
  { "tbcctl3",	"188", 0x188 },
  { "tbcctl4",	"18A", 0x18A },
  { "tbcctl5",	"18C", 0x18C },
  { "tbcctl6",	"18E", 0x18E },
  { "tbr",	"190", 0x190 },
  { "tbccr0",	"192", 0x192 },
  { "tbccr1",	"194", 0x194 },
  { "tbccr2",	"196", 0x196 },
  { "tbccr3",	"198", 0x198 },
  { "tbccr4",	"19A", 0x19A },
  { "tbccr5",	"19C", 0x19C },
  { "tbccr6",	"19E", 0x19E },
  { "adc12ctl0","1A0", 0x1A0 },
  { "adc12ctl1","1A2", 0x1A2 },
  { "adc12fg",	"1A4", 0x1A4 },
  { "adc12ie",	"1A6", 0x1A6 },
  { 0,          0,     0 }
};

}
