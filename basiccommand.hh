/* Copyright (c) 2002 Dirk Jagdmann <doj@cubic.org>, Michael Stickel <michael@stickel.org> */

/* This software is provided 'as-is', without any express or implied */
/* warranty. In no event will the authors be held liable for any damages */
/* arising from the use of this software. */

/* Permission is granted to anyone to use this software for any purpose, */
/* including commercial applications, and to alter it and redistribute it */
/* freely, subject to the following restrictions: */

/*     1. The origin of this software must not be misrepresented; you */
/*        must not claim that you wrote the original software. If you use */
/*        this software in a product, an acknowledgment in the product */
/*        documentation would be appreciated but is not required. */

/*     2. Altered source versions must be plainly marked as such, and */
/*        must not be misrepresented as being the original software. */

/*     3. This notice may not be removed or altered from any source */
/*        distribution. */

/* $Header: /code/bslsh/basiccommand.hh,v 1.6 2010/05/22 01:23:25 doj Exp $ */

#ifndef BASICCOMMAND__HH
#define BASICCOMMAND__HH

#include <cstdio>

#include "debug.h"
#include "serial.hh"

namespace MSP430 {

  namespace BSL {

    class BasicCommand
    {
    protected:
      Serial serial;

#if __GNUC__ >= 3
#pragma pack ( push, 1 )
#endif
      struct frame_t {
	BYTE HDR;
	BYTE CMD;
	BYTE L1;
	BYTE L2;
	BYTE AL;
	BYTE AH;
	BYTE LL;
	BYTE LH;
	BYTE data[250];
      }
#if __GNUC__ < 3
__attribute__ ((packed))
#endif
      ;
#if __GNUC__ >= 3
#pragma pack ( pop )
#endif

    public:
      BasicCommand(const std::string& device) :
	serial(device)
      { }

      bool open(const std::string& device)
      {
	return serial.connect(device);
      }

      bool connected()
      {
	return serial.connected();
      }

      bool transmit(const BYTE *data, const size_t dataLen, const WORD addr);
      bool password(const BYTE *data);
      bool eraseSegment(const WORD addr);
      bool eraseAll();
      bool loadpc(const WORD addr);
      bool receive(const WORD addr, const size_t len, BYTE *buf);

      bool reset(bool invokeBSL=false)
      {
	VERBOSE("BasicCommand::reset()\n");
	return serial.reset(invokeBSL);
      }

    private:
      BasicCommand();
    };

  } // namespace BSL

} // namespace MSP430

#endif // BASICCOMMAND__HH
