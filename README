bslsh is a program to upload code and to inspect the system of MSP430 devices
through the BSL interface. You can think of it as a primitive system monitor.
bslsh currently features:

- code/data upload/flashing
- disassembly of memory contents
- poke/peek of memory
- execute code
- dump memory
- fill memory
- erase memory/flash

See INSTALL how to build and install the software.
The bslsh is free and open source software covered by the zlib/libpng license.

To interface the MSP430 hardware you need to build a simple interface
for rs232 to 3.3V voltage conversion. The schematic is found in the
schem/ directory. The msp430_bsl.* schematics were provided by
TBS::Antiemes. You can use the freeware version of Eagle to work with
the schematic files: http://cadsoft.de/

Below is a short documentation for bslsh:

Command line options:
--device <dev>|-d <dev> use the serial port <dev> for accessing the target
--bsl                   start BSL upon startup
--eraseall              erase complete flash memory
--flash <file>          flash <file>
--passwordfile <file>   <file> contains 32 bytes that represents the password
--go <addr>|"rv"        execute at addr or standard reset vector
--reset                 make a reset
--debug <level>         set debug level to <level>
--quiet                 dont say anything except the data output
--verbose               do some more output
--help | -?             show this help

To use bslsh you have to supply the right serial device with the "--device"
parameter. To use the second serial port start bslsh with
"bslsh --device /dev/ttyS1"

Now you have entered the shell. You can now type the command listed below:
help     [<command>]         Shows help
?        [<command>]         Shows help
call     <addr>              Call a function at <addr>
cd       <dir>               change current directory to dir
deek     <addr>              Reads a word from address <addr>
disas    <range> [<file>]    Disassembles the memory in <range>, default size 16
doke     <addr> <word>       Write a word at address <word>
dump     <range> [<file>]    Dumps memory in <range>, default size 16
erase    <addr> | "all"      Erase a flash segment (handle with care!)
fill     <range> <byte>      fill memory block with byte, default size 1
flash    <filename> ["swap"] flash <filename> into target
go       <addr>|"rv"         Execute code at <addr> or standard reset vector
ls       [<arg>*]            execute the ls program
password <file>              read password from file and transmit to target
peek     <addr>              Reads a byte from address <addr>
poke     <addr> <byte>       Writes a byte at the address <addr>
quiet    "on" | "off"        enable quiet mode
quit                         Exit this application
q                            Exit this application
reset    ["bsl"]             Resets the target
run      <addr>|"rv"         Execute code at <addr> or standard reset vector
status                       Print status information
verbose  "on" | "off"        enable verbose mode
version                      print version

All numbers are parsed in c-style syntax being decimal by
default, except when prefixed with "0x" which indicates a hex or "0"
which indicates an octal format.

<addr> is a number indicating a adress in MSP430 memory.
<file> is a filename on the local filesystem.
"string" is a string used to modify the default command semantics.
[opt] is an optional parameter which may be omitted.

<range> defines a range in MSP430 memory. It may be given in two
forms.  You can either use two explicit addresses to define the range
like "1024 - 0x1FFF". Note that the "-" character must be surrounded
by whitespace!  Or you can define the range with a start and length
syntax "0x400 7168".  In this examples both forms define the same
range.

Please note that you have to transmit a password when invoking the
shell if the password area of the MSP430 is set. You can use the
password command in the shell or the command line option to set the
password.

We now make a sample session using the bslsl.

1) On the (bash) command line we start the bslsh program using serial
port 1: "bslsh --device /dev/ttyS1"

2) We invoke the BSL on the MSP430:
"reset bsl"

3) We erase all memory:
"erase all"

4) We fill the memory with some value:
"fill 0x200 - 0x2ff 12"

5) We can check if the memory has been filled:
"peek 0x200"

6) We can see what program is executed with these commands:
"disas 0x200 0x100"

7) Now we upload some program:
"flash test.hex"

8) assuming that test.hex starts at 0xF000 we start it:
"go 0xf000"

You can use the command line interface of bslsh for batch style
flashing and upload. To flash and start a program use bslsh as
follows:

bslsh --device /dev/ttyS1 --bsl --eraseall --flash test.hex --reset

If the code in test.hex installs a reset vector in the vector table of
the MSP430 the program is started with the final "--reset" option.


For further questions, comments, patches and whatever please contact
the authors:
Dirk Jagdmann <doj@cubic.org>
Michael Stickel <michael@stickel.org>

Links to MSP430 development software:
 * ASL assembler: http://john.ccac.rwth-aachen.de:8000/as/
                  http://john.ccac.rwth-aachen.de:8000/ftp/as/source/c_version/
 * The GCC toolchain for the MSP430: http://mspgcc.sourceforge.net/
