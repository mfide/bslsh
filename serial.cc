/* Copyright (c) 2002 Dirk Jagdmann <doj@cubic.org>, Michael Stickel <michael@stickel.org> */

/* This software is provided 'as-is', without any express or implied */
/* warranty. In no event will the authors be held liable for any damages */
/* arising from the use of this software. */

/* Permission is granted to anyone to use this software for any purpose, */
/* including commercial applications, and to alter it and redistribute it */
/* freely, subject to the following restrictions: */

/*     1. The origin of this software must not be misrepresented; you */
/*        must not claim that you wrote the original software. If you use */
/*        this software in a product, an acknowledgment in the product */
/*        documentation would be appreciated but is not required. */

/*     2. Altered source versions must be plainly marked as such, and */
/*        must not be misrepresented as being the original software. */

/*     3. This notice may not be removed or altered from any source */
/*        distribution. */

/* $Header: /code/bslsh/serial.cc,v 1.10 2010/05/22 01:23:26 doj Exp $ */

#include <cstdio>

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/types.h>
//#include <sys/termios.h>
#include <termios.h>
#include <unistd.h>

#include <iostream>

#include "debug.h"
#include "serial.hh"
#include "tools.hh"

using namespace std;

namespace MSP430 {

  namespace BSL {

    bool Serial::connect(const string& device)
    {
      disconnect();

      fh = ::open (device.c_str(), O_RDWR);
      if (fh < 0)
	{
	  VERBOSE("could not open %s\n", device.c_str());
	  return false;
	}

      // set up the port with 9600,8,EVENPARITY,ONESTOPBIT
      struct termios my_tios;

      tcgetattr   (fh, &my_tios);
      cfmakeraw   (&my_tios);

      cfsetispeed (&my_tios, B9600); // 9600 baud
      cfsetospeed (&my_tios, B9600);

      my_tios.c_cflag &= ~CSIZE;
      my_tios.c_cflag |= CS8;  // 8bit

      my_tios.c_cflag &= ~(PARENB|PARODD);
      my_tios.c_cflag |= PARENB; // even parity

      my_tios.c_cflag &=~CSTOPB;  // 1 stopbit
      my_tios.c_cflag &=~CRTSCTS; // no RTS-CTS

      tcsetattr   (fh, TCSANOW, &my_tios);

      // apply power to the interface
      RST(1);
      TEST(0);

      return true;
    }

    bool Serial::connected() const
    {
      return fh>=0;
    }

    bool Serial::RST(bool level)
    {
      int pin = TIOCM_DTR;
      // reset pin is inverted in serial->bsl converter
      if (ioctl(fh, level?TIOCMBIC:TIOCMBIS, &pin) < 0)
	{
	  perror("Serial::RST(): could not set RST");
	  return false;
	}
      return true;
    }


    /**
     *
     * Controls TEST pin (0: VCC; 1: GND)
     */
    bool Serial::TEST(bool level)
    {
      int pin = TIOCM_RTS;
      // test pin is inverted in serial->bsl converter
      if (ioctl(fh, level?TIOCMBIC:TIOCMBIS, &pin) < 0)
	{
	  perror("Serial::TEST(): could not set TEST");
	  return false;
	}
      return true;
    }

    bool Serial::reset(bool invokeBSL)
    {
      VERBOSE("Serial::reset()\n");

      RST(0);
      TEST(0);
      delay(2500);

      if (invokeBSL)
	{
	  TEST(1);
	  delay(10);
	  TEST(0);
	  delay(20);
	  TEST(1);
	  delay(10);
	  RST (1);
	  delay(10);
	  TEST(0);
	}
      else
	RST(1);

      /* Give MSP430's oscillator time to stabilize: */
      delay(2500);

      /* Clear buffers: */
      clearBuffer();

      return true;
    }

    /**
     *
     * delays execution for s useconds.
     */
    void Serial::delay(const unsigned s) const
    {
      struct timeval tv;
      tv.tv_sec = s/1000000;
      tv.tv_usec = s%1000000;
      select (0,NULL,NULL,NULL, &tv);
    }

    /** read any bytes in the serial buffer */
    bool Serial::clearBuffer()
    {
      VERBOSE("Serial::clearBuffer()\n");

      if(!connected())
	return false;

      while (true)
	{
	  fd_set  rxset;
	  FD_ZERO(&rxset);
	  FD_SET(fh, &rxset);

	  struct timeval tv;
	  tv.tv_sec=0;
	  tv.tv_usec=10000;

	  int stat = select (fh+1, &rxset, NULL, NULL, &tv);
	  if (stat < 0)
	    {
	      perror("Serial:clearBuffer(): error or signal in select");
	      return false;
	    }
	  else if(stat == 0)
	    break;
	  else
	    {
	      if(FD_ISSET(fh, &rxset))
		{
		  char c;
		  ::read(fh, &c, 1);
		  VERBOSE("Serial::clearBuffer(): read %02X", c);
		}
	    }
	}
      return true;
    }

    bool Serial::disconnect()
    {
      VERBOSE("Serial::disconnect()\n");

      if(connected())
	{
	  if(::close(fh) < 0)
	    {
	      perror("Serial::disconnect(): could not close");
	      return false;
	    }
	  fh=-1;
	  return true;
	}
      return false;
    }

    bool Serial::transmit(const BYTE *frame, const size_t frameLen)
    {
      VERBOSE("Serial::transmit()\n");

      if(!connected())
	{
	  cerr << "Serial::transmit(): not connected. aborting..." << endl;
	  return false;
	}

      if(frame==NULL)
	{
	  cerr << "Serial::transmit(): frame==NULL. aborting..." << endl;
	  return false;
	}

      if(frameLen<8)
	{
	  cerr << "Serial::transmit(): len<8. aborting..." << endl;
	  return false;
	}

      if(frameLen&1)
	{
	  cerr << "Serial::transmit(): len is odd. aborting..." << endl;
	  return false;
	}

      clearBuffer();

      size_t i, w, len;
      BYTE b;

      for(i=0; i<3; i++)
	{
	  VERBOSE("Serial::transmit(): write SYNC\n");
	  b=SYNC;
	  w=::write(fh, &b, 1);
	  if(w <= 0)
	    {
	      perror("Serial::transmit(): could not write sync");
	      return false;
	    }

	  VERBOSE("Serial::transmit(): read ACK\n");
	  len=1;
	  if(!read(&b, len))
	    continue;
	  if(len!=1 || b!=DATA_ACK)
	    {
	      cerr << "Serial::transmit(): could not read SYNC ACK" << endl;
	      return false;
	    }
	  break;
	}
      if(i>=3)
	{
	  cerr << "Serial::transmit(): tried " << i << " times to receive ACK. aborting..." << endl;
	  return false;
	}

      // calculate checksum
      BYTE CKL, CKH;
      i=0;
      CKL=frame[i++];
      CKH=frame[i++];
      while(i<frameLen)
	{
	  CKL^=frame[i++];
	  CKH^=frame[i++];
	}
      CKL=~CKL;
      CKH=~CKH;

      // transmit frame
      VERBOSE("Serial::transmit(): write frame\n");
      w=::write(fh, frame, frameLen);
      if(w < 0)
	{
	  perror("Serial::transmit(): could not write data");
	  return false;
	}
      else if(w<frameLen)
	{
	  cerr << "Serial::transmit(): could not write complete data" << endl;
	  return false;
	}

      VERBOSE("Serial::transmit(): write checksum\n");
      w=::write(fh, &CKL, 1);
      if(w < 1)
	{
	  perror("Serial::transmit(): could not write CKL");
	  return false;
	}

      w=::write(fh, &CKH, 1);
      if(w < 1)
	{
	  perror("Serial::transmit(): could not write CKH");
	  return false;
	}

      // read ack
      VERBOSE("Serial::transmit(): read ACK\n");
      len=1;
      if(!read(&b, len))
	return false;
      if(len!=1)
	{
	  cerr << "Serial::transmit(): could not read data ACK" << endl;
	  return false;
	}

      switch(b)
	{
	case DATA_NAK:
	  cerr << "Serial::transmit(): Frame not valid" << endl;
	  return false;
	case CMD_FAILED:
	  cerr << "Serial::transmit(): Command is not defined or not allowed" << endl;
	  return false;
	}

      return true;
    }

    bool Serial::receive(const BYTE *frame, const size_t frameLen, BYTE *buf)
    {
      // check parameters
      if(!connected())
	{
	  cerr << "Serial::receive(): not connected. aborting..." << endl;
	  return false;
	}

      if(frame==NULL)
	{
	  cerr << "Serial::receive(): frame==NULL. aborting..." << endl;
	  return false;
	}

      if(frameLen!=8)
	{
	  cerr << "Serial::receive(): frameLen=" << frameLen << ". len=8 expected. aborting..." << endl;
	  return false;
	}

      // transmit request frame
      if(!transmit(frame, frameLen))
	{
	  cerr << "Serial::receive(): could not transmit frame. aborting..." << endl;
	  goto receiveerror;
	}

      // receive result. a SYNC (0x80) was implicitely received by the transmit() method
      BYTE CMD;
      size_t len;
      len=1;
      if(!read(&CMD, len))
	  goto receiveerror;
      if(len!=1)
	{
	  cerr << "Serial::receive(): could not read CMD" << endl;
	  goto receiveerror;
	}

      BYTE L1;
      len=1;
      if(!read(&L1, len))
	  goto receiveerror;
      if(len!=1)
	{
	  cerr << "Serial::receive(): could not read L1" << endl;
	  goto receiveerror;
	}

      BYTE L2;
      len=1;
      if(!read(&L2, len))
	  goto receiveerror;
      if(len!=1)
	{
	  cerr << "Serial::receive(): could not read L2" << endl;
	  goto receiveerror;
	}

      if(L1!=L2)
	{
	  cerr << "Serial::receive(): L1 and L2 do not match" << endl;
	}

      if(L1&1)
	{
	  cerr << "Serial::receive(): L1 is odd" << endl;
	}

      if(L1>=255)
	{
	  cerr << "Serial::receive(): L1 is >= 255" << endl;
	}

      VERBOSE("Serial::receive(): L1==%i\n", L1);

      len=L1;
      if(!read(buf, len))
	  goto receiveerror;
      if(len!=L1)
	{
	  cerr << "Serial::receive(): read " << len << " bytes, while " << L1 << " bytes exepcted" << endl;
	  goto receiveerror;
	}

      BYTE CKL;
      len=1;
      if(!read(&CKL, len))
	  goto receiveerror;
      if(len!=1)
	{
	  cerr << "Serial::receive(): could not read CKL" << endl;
	  goto receiveerror;
	}

      BYTE CKH;
      len=1;
      if(!read(&CKH, len))
	  goto receiveerror;
      if(len!=1)
	{
	  cerr << "Serial::receive(): could not read CKH" << endl;
	  goto receiveerror;
	}

      // calc checksum
      BYTE ckl, ckh;
      ckl=SYNC; ckh=CMD;
      ckl^=L1;  ckh^=L2;
      int i;
      i=0;
      while(i<L1)
	{
	  ckl^=buf[i++];
	  ckh^=buf[i++];
	}
      ckl=~ckl;
      ckh=~ckh;

      if(ckl!=CKL || ckh!=CKH)
	{
	  cerr << "Serial::receive(): checksum error low: ";
	  HEX(cerr, ckl, 2) << "=";
	  HEX(cerr, CKL, 2) << " high: ";
	  HEX(cerr, ckh, 2) << "=";
	  HEX(cerr, CKH, 2) << endl;
	  //return false;
	}

      return true;

    receiveerror:

      clearBuffer();
      return false;

    }

    bool Serial::read(BYTE *data, size_t& len)
    {
      if(!connected())
	return false;

      const size_t total=len;
      len=0;
      while(len<total)
	{
	  fd_set  rxset;
	  FD_ZERO(&rxset);
	  FD_SET(fh, &rxset);

	  struct timeval tv;
	  tv.tv_sec=(len/2)?len/2:1; // adapt timeout to length of buffer
	  tv.tv_usec=0;
	  const int stat = select (fh+1, &rxset, NULL, NULL, &tv);
	  if (stat < 0)
	    {
	      perror("Serial::read(): error or signal in select");
	      return false;
	    }
	  else if(stat == 0)
	    {
	      cerr << "Serial::read(): timeout in select" << endl;
	      return false;
	    }
	  else
	    {
	      if(FD_ISSET(fh, &rxset))
		{
		  const size_t r=::read(fh, &data[len], total-len);
		  VERBOSE("Serial::read(): read %i bytes\n", r);
		  len+=r;
		}
	    }
	}

      return true;
    }

  }

}
