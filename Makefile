USE_READLINE := 1

PREFIX?=	/usr/local

CXXFLAGS+=	-DDOJDEBUG=1 -g
CXXFLAGS+=	-Wall
#CXXFLAGS+=	-O2

CXXFLAGS+=	-I/usr/local/include # -I/usr/local/include/srecord -I/usr/include/srecord
LIBS+=		-lsrecord

ifeq ($(USE_READLINE), 1)
CXXFLAGS+=	-DUSE_READLINE=1
LIBS+=		-lreadline -ltermcap
endif

all : bslsh dis430
	$(MAKE) Makefile.dep

bslsh : shell.o disassembler.o registername.o serial.o basiccommand.o command.o tools.o
	$(CXX) $(LDFLAGS) -o $@ $^ $(LIBS)

dis430:	dis430.o disassembler.o registername.o tools.o
	$(CXX) $(LDFLAGS) -o $@ $^ $(LIBS)

clean:
	rm -f *~ bslsh *.o core TAGS *.bak dis430 Makefile.dep

Makefile.dep:
	$(CXX) -MM $(CXXFLAGS) *.cc > $@

tags:
	etags *.cc

install:
	install -m 755 bslsh $(PREFIX)/bin/
	install -m 755 dis430 $(PREFIX)/bin/

distclean:	clean
	rm -f $(PREFIX)/bin/bslsh
	rm -f $(PREFIX)/bin/dis430

-include Makefile.dep
