/* Copyright (c) 2002 Dirk Jagdmann <doj@cubic.org>, Michael Stickel <michael@stickel.org> */

/* This software is provided 'as-is', without any express or implied */
/* warranty. In no event will the authors be held liable for any damages */
/* arising from the use of this software. */

/* Permission is granted to anyone to use this software for any purpose, */
/* including commercial applications, and to alter it and redistribute it */
/* freely, subject to the following restrictions: */

/*     1. The origin of this software must not be misrepresented; you */
/*        must not claim that you wrote the original software. If you use */
/*        this software in a product, an acknowledgment in the product */
/*        documentation would be appreciated but is not required. */

/*     2. Altered source versions must be plainly marked as such, and */
/*        must not be misrepresented as being the original software. */

/*     3. This notice may not be removed or altered from any source */
/*        distribution. */

/* $Header: /code/bslsh/disassembler.hh,v 1.7 2010/05/22 01:23:26 doj Exp $ */

#ifndef DISASSEMBLER__HH
#define DISASSEMBLER__HH

#include <iostream>
#include <set>

#include "tools.hh"

namespace MSP430
{

  /**
     Disassemble assembly source for the MSP430 processor.

     WARNING! This class uses no range checking or pointer checking!
     Please provide valid pointers and addresses when using this
     class. Note that the disassembling process may bounce over the
     valid range of the memory area. If the program counter (pc)
     increases beyong 0xFFFF (therefore becoming 0x000) the return
     results from the disassemble() methods may be incorrect.
  */
  class Disassembler
  {
  public:
    /// memory used to disassemble
    union {
      WORD *memory;
      BYTE *mem;
    };
    /// program counter which is used to locate the current statement in memory
    WORD pc;

    /// the total number of cycles used by the last disassembly run
    int cycles;

  public:
    /**
       Instantiate and initialize a new Disassembler. The Disassembler
       needs a block of memory which should include the memory dump of
       the MSP430 processor to disassemble. The Disassembler maintains
       its own internal program counter to ease the disassembling
       process.

       You can set the member variables memory and pc with this
       Constructor or directly, as they are exported public.
    */
    Disassembler(void *mem=NULL, const WORD programcounter=0) :
      memory(static_cast<WORD*>(mem)),
      pc(programcounter),
      cycles(0)
    { }

    /**
       disassembles one statement onto os. If addr is set, the program
       counter is replaced by addr before disassembling. After
       execution the program counter will point to the next
       statement. Repeatingly calling this function will disasseble
       through the entire memory.

       @param os output stream
       @param addr new value for program counter. If program counter should not be modified use -1

       @return the number of bytes the disassembled statement uses in program memory, -1 on error
     */
    int disassemble(std::ostream& os=std::cout, const int addr=-1);

    /**
       disassemble of block of memory. Start disassembling at addr and then disassemble at statements using at least len bytes in memory. WARNING! This method may use (read) more bytes than len! This may result to a SIGSEV.

       @param len range in memory (together with addr) to disassemble
       @param addr start address to start from. the program counter is set to this value.
       @param os output stream

       @return the number of bytes used for disassembling, -1 on error
    */
    int disassemble(int len, const int addr=-1, std::ostream& os=std::cout);

    /**
       resets the disassembler. pc is set to 0 and the jumptable is cleared.
    */
    void reset()
    {
      pc=0;
      jmpdst.clear();
    }

  protected:
    static const char *registerNames[16];
    static const char *twoOpNames[16];
    static const char *singleOpNames[8];
    static const char *jumpOpNames[8];

    typedef std::set<WORD> jmpdst_t;
    jmpdst_t jmpdst;

    enum operand_t {
      Const0, Const1, Const2, Const4, Const8, Constm1,
      Register,
      Number,
    };

    enum twoOp_t {
      twoOp0 = 0, twoOp1, twoOp2, twoOp3,
      MOV, ADD, ADDC, SUBC,
      SUB, CMP, DADD, BIT,
      BIC, BIS, XOR,  AND
    };

    enum oneOp_t {
      RRC = 0, SWPB, RRA, SXT,
      PUSH, CALL, RETI
    };

    enum Register_t {
      PC=0, SP,  SR,
    };

    enum AddressingMode_t {
      RegisterMode,
      ConstantMode,
      IndexedMode,
      SymbolicMode,
      AbsoluteMode,
      IndirectMode,
      AutoincrementMode,
      ImediateMode,
    };

    operand_t lastoperand;
    AddressingMode_t lastaddressingmode;
    int lastabsoluteaddress;
    int currentcycle;

    int decode_operand (const int am, const int reg, std::ostream& os=std::cout);

    WORD fetch()
    {
      WORD w=memory[pc/2];
      pc+=sizeof(WORD);
      return w;
    }
  };

} // namespace MSP430

#endif // DISASSEMBLER__HH
