/* Copyright (c) 2002 Dirk Jagdmann <doj@cubic.org>, Michael Stickel <michael@stickel.org> */

/* This software is provided 'as-is', without any express or implied */
/* warranty. In no event will the authors be held liable for any damages */
/* arising from the use of this software. */

/* Permission is granted to anyone to use this software for any purpose, */
/* including commercial applications, and to alter it and redistribute it */
/* freely, subject to the following restrictions: */

/*     1. The origin of this software must not be misrepresented; you */
/*        must not claim that you wrote the original software. If you use */
/*        this software in a product, an acknowledgment in the product */
/*        documentation would be appreciated but is not required. */

/*     2. Altered source versions must be plainly marked as such, and */
/*        must not be misrepresented as being the original software. */

/*     3. This notice may not be removed or altered from any source */
/*        distribution. */

/* $Header: /code/bslsh/tools.cc,v 1.5 2010/05/22 01:23:26 doj Exp $ */

#include "streamstate.hh"
#include "tools.hh"

std::ostream& HEX(std::ostream& os, unsigned int i, size_t width)
{
  std::Streamstate state(os);
  os << std::hex; //  << std::uppercase;
  os.width(width);
  os.fill('0');
  os << i;
  state.set(os);
  return os;
}
