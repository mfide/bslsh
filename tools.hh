/* Copyright (c) 2002 Dirk Jagdmann <doj@cubic.org>, Michael Stickel <michael@stickel.org> */

/* This software is provided 'as-is', without any express or implied */
/* warranty. In no event will the authors be held liable for any damages */
/* arising from the use of this software. */

/* Permission is granted to anyone to use this software for any purpose, */
/* including commercial applications, and to alter it and redistribute it */
/* freely, subject to the following restrictions: */

/*     1. The origin of this software must not be misrepresented; you */
/*        must not claim that you wrote the original software. If you use */
/*        this software in a product, an acknowledgment in the product */
/*        documentation would be appreciated but is not required. */

/*     2. Altered source versions must be plainly marked as such, and */
/*        must not be misrepresented as being the original software. */

/*     3. This notice may not be removed or altered from any source */
/*        distribution. */

/* $Header: /code/bslsh/tools.hh,v 1.7 2010/05/22 01:23:26 doj Exp $ */

#ifndef TOOLS__HH
#define TOOLS__HH

#include <iostream>

#if defined(__i386__) || defined(__ARM__)
typedef unsigned char BYTE;
typedef unsigned short WORD;
#else
#error please define your platform for types in tools.hh
#endif

inline WORD BE2HOST(const WORD w)
{
#if 0
#if defined(__i386__) || defined(__ARM__)
  const BYTE *b=(const BYTE*)(&w);
  return (b[0]<<8)+b[1];
#else
#error please define your platform for BE2HOST() in tools.hh
#endif

#else

  return w;

#endif
}

inline WORD HOST2BE(const WORD w)
{
  return BE2HOST(w);
}

std::ostream& HEX(std::ostream& os, unsigned int i, size_t width=4);

#endif
