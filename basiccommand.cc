/* Copyright (c) 2002 Dirk Jagdmann <doj@cubic.org>, Michael Stickel <michael@stickel.org> */

/* This software is provided 'as-is', without any express or implied */
/* warranty. In no event will the authors be held liable for any damages */
/* arising from the use of this software. */

/* Permission is granted to anyone to use this software for any purpose, */
/* including commercial applications, and to alter it and redistribute it */
/* freely, subject to the following restrictions: */

/*     1. The origin of this software must not be misrepresented; you */
/*        must not claim that you wrote the original software. If you use */
/*        this software in a product, an acknowledgment in the product */
/*        documentation would be appreciated but is not required. */

/*     2. Altered source versions must be plainly marked as such, and */
/*        must not be misrepresented as being the original software. */

/*     3. This notice may not be removed or altered from any source */
/*        distribution. */

/* $Header: /code/bslsh/basiccommand.cc,v 1.6 2010/05/22 01:23:25 doj Exp $ */

#include <cstring>
#include <iostream>

#include "basiccommand.hh"

using namespace std;

namespace MSP430 {

  namespace BSL {

    bool BasicCommand::transmit(const BYTE *data, const size_t dataLen, const WORD addr)
    {
      VERBOSE("BasicCommand::transmit(len=%02X, addr=%04X)\n", dataLen, addr);

      if(data==NULL)
	{
	  cerr << "BasicCommand::transmit(): data==NULL" << endl;
	  return false;
	}

      if(dataLen>250)
	{
	  cerr << "BasicCommand::transmit(): dataLen==" << dataLen << " which exceeds maximum of 250" << endl;
	  return false;
	}

      if(addr+dataLen>0x10000)
	{
	  cerr << "BasicCommand::transmit(): addr+dataLen>0x10000" << endl;
	  return false;
	}

      struct frame_t f;
      f.HDR=0x80;
      f.CMD=0x12;
      f.L1=dataLen+4;
      f.L2=dataLen+4;
      f.AL=addr&0xFF;
      f.AH=(addr>>8)&0xFF;
      f.LL=dataLen;
      f.LH=0;
      memcpy(f.data, data, dataLen);

      return serial.transmit(reinterpret_cast<BYTE*>(&f), f.L1+4);
    }

    bool BasicCommand::password(const BYTE *data)
    {
      VERBOSE("BasicCommand::password()\n");

      if(data==NULL)
	{
	  cerr << "BasicCommand::password(): data==NULL. aborting..." << endl;
	  return false;
	}

      struct frame_t f;
      f.HDR=0x80;
      f.CMD=0x10;
      f.L1=0x24;
      f.L2=0x24;
      memcpy(f.data, data, 32);

      return serial.transmit(reinterpret_cast<BYTE*>(&f), f.L1+4);
    }

    bool BasicCommand::eraseSegment(const WORD addr)
    {
      VERBOSE("BasicCommand::eraseSegment()\n");

      struct frame_t f;
      f.HDR=0x80;
      f.CMD=0x16;
      f.L1=4;
      f.L2=4;
      f.AL=addr&0xFF;
      f.AH=(addr>>8)&0xFF;
      f.LL=0x02;
      f.LH=0xA5;
      return serial.transmit(reinterpret_cast<BYTE*>(&f), f.L1+4);
    }

    bool BasicCommand::eraseAll()
    {
      VERBOSE("BasicCommand::eraseAll()\n");

      struct frame_t f;
      f.HDR=0x80;
      f.CMD=0x18;
      f.L1=4;
      f.L2=4;
      return serial.transmit(reinterpret_cast<BYTE*>(&f), f.L1+4);
    }

    bool BasicCommand::loadpc(const WORD addr)
    {
      VERBOSE("BasicCommand::loadpc()\n");

      struct frame_t f;
      f.HDR=0x80;
      f.CMD=0x1A;
      f.L1=4;
      f.L2=4;
      f.AL=addr&0xFF;
      f.AH=(addr>>8)&0xFF;
      return serial.transmit(reinterpret_cast<BYTE*>(&f), f.L1+4);
    }

    bool BasicCommand::receive(const WORD addr, const size_t len, BYTE *buf)
    {
      VERBOSE("BasicCommand::receive()\n");

      if(addr+len>0x10000)
	{
	  cerr << "BasicCommand::receive(): addr+len > 0x10000" << endl;
	  return false;
	}

      if(buf==NULL)
	{
	  cerr << "BasicCommand::receive(): buf==NULL" << endl;
	  return false;
	}

      struct frame_t f;
      f.HDR=0x80;
      f.CMD=0x14;
      f.L1=4;
      f.L2=4;
      f.AL=addr&0xFF;
      f.AH=(addr>>8)&0xFF;
      f.LL=len;
      f.LH=0;
      return serial.receive(reinterpret_cast<BYTE*>(&f), f.L1+4, buf);
    }

  }

}
